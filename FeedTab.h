//
//  FeedTab.h
//  inTown
//
//  Created by Mahbub Morshed on 6/9/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedTab : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    UITableView *friendFeedTableView;
    UITableView *myFeedTableView;
    
}

@property(nonatomic,retain) IBOutlet UITableView *friendFeedTableView;
@property(nonatomic,retain) IBOutlet UITableView *myFeedTableView;

@end
