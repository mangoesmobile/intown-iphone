//
//  AUITableViewController.h
//  inTown
//
//  Created by Mahbub Morshed Prottoy on 9/8/11.
//  Copyright 2011 Mahbub Morshed Prottoy. All rights reserved.
//

#import "TKCalendarMonthTableViewController.h"
#import <UIKit/UIKit.h>

@interface AUITableViewController : UITableViewController{
    NSMutableArray *cityData ;
    NSMutableArray *colorData;
    NSMutableArray *colorToAdd;
}

@end
