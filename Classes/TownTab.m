//
//  TownTab.m
//  inTown
//
//  Created by ManGoes Mobile 2 on 20/8/11.
//  Copyright 2011 Mahbub Moshed. All rights reserved.
//

#import "TownTab.h"


@implementation TownTab

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //[[self navigationItem] setTitle:@"Towns"];
        
        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Cooper Black" size:35];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
        label.text = NSLocalizedString(@"Towns", @"");
        [label sizeToFit];
//        [[self navigationItem] ];        	
        //self.navigationController.navigationBarHidden= YES;
        
        //self.view.backgroundColor= [UIColor purpleColor];
        
        theTableView= [[theTableView init] alloc];
        
        theTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) style:UITableViewStylePlain];
        //[theTableView setDelegate:twnViewController];
        //[theTableView setDataSource:twnViewController];
        [theTableView setDelegate:self];
        [theTableView setDataSource:self];
        [theTableView setRowHeight:100];
        [theTableView setBackgroundColor:[UIColor blackColor]];
        [theTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        theTableView.showsVerticalScrollIndicator= NO;
        [theTableView setSeparatorColor:[UIColor lightGrayColor]];
        [theTableView setAllowsSelection:YES];
        
        [self.view addSubview:theTableView];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // [TownViewController release];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
     self.navigationController.navigationBarHidden = YES;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
   
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Edit" style: UIBarButtonItemStylePlain target:self action:@selector(nextView) ]autorelease]; 
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStylePlain target:self action:@selector(selectCalendar) ]autorelease]; 
}

- (void)nextView{
    
    UIViewController *targetViewController = [[[TownsSub2 alloc]init ] autorelease]; 
    [[self navigationController] pushViewController:targetViewController animated:YES];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)viewWillAppear:(BOOL)animated{
       	self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc
{
    [theTableView release];
    [super dealloc];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    //cell.textLabel.text=@"Name of Town";
    //cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:12.0];;
    
    // Configure the cell...
    
    CGRect Label1Frame = CGRectMake(10, 10, 290, 25);
    UILabel *lbl1;
    
    lbl1 = [[UILabel alloc] initWithFrame:Label1Frame];
    lbl1.tag = 1;
    lbl1.font= [UIFont fontWithName:@"Helvetica" size:15];
    lbl1.textColor= [UIColor lightGrayColor];
    
    lbl1.text=@"SAN FRANCISCO, CA";
    [lbl1 setBackgroundColor:[UIColor blackColor]];
	[cell.contentView addSubview:lbl1];
	[lbl1 release];
    
    CGRect Label2Frame = CGRectMake(10, 30, 290, 25);
    UILabel *lbl2;
    
    lbl2 = [[UILabel alloc] initWithFrame:Label2Frame];
	lbl2.tag = 1;
    lbl2.font= [UIFont fontWithName:@"Helvetica" size:12];
    lbl2.textColor= [UIColor lightGrayColor];
    
    lbl2.text=@"Jan 1- Jan 3";
    [lbl2 setBackgroundColor:[UIColor blackColor]];
	[cell.contentView addSubview:lbl2];
	[lbl2 release];
    
    
    UIScrollView *scroll = [[[UIScrollView alloc] initWithFrame: CGRectMake(35, 50, 250, 100)] autorelease];
    scroll.contentSize = CGSizeMake(500, 50);
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator= NO;
    scroll.pagingEnabled=YES;
    [scroll setTag:100];
    
    //[cell.contentView removeChildWithTag: 100];
    [cell.contentView addSubview:scroll];
    
    
    
    //[addCityView addSubview:scroll];
    //[scroll addSubview:lblTemp];
    
    for (int i=0 ; i<10 ; i++) {
        
        NSString *stringName=@"profilepic.png";
        
        if(i%2 ==0){
            stringName=@"profilepic1.png";
        }
        
        
        UIImageView *btnImage1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:stringName] ];
        btnImage1.frame= CGRectMake((i*50) , 0, 50, 50);
        scroll.backgroundColor= [UIColor clearColor];
        
        [scroll addSubview:btnImage1];
        [btnImage1 release];
    }
    
    
    
    //[color1 release];
    
    //    [scroll release];
    
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    UIViewController *targetViewController = [[[TownsSub1 alloc]init ] autorelease]; 
    //[self.tableView removeFromSuperview];
    [[self navigationController] pushViewController:targetViewController animated:YES];
    
}

-(void)selectCalendar{
    [self.tabBarController setSelectedIndex:2];
}



@end
