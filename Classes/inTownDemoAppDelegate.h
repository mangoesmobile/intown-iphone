//
//  inTownDemoAppDelegate.h
//  inTown
//
//  Created by Mahbub Morshed on 6/8/11.
//  Copyright 2011 Mahbub Morshed All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ApplicationTabBarController.h"
#import "TutorialScreen.h"


@interface inTownDemoAppDelegate : NSObject <UIApplicationDelegate, FBSessionDelegate, FBRequestDelegate> {
    UIWindow *window;
	ApplicationTabBarController *tabBarController;
    TutorialScreen *tutorial;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

