//
//  FeedTabSub1.h
//  inTown
//
//  Created by Mahbub Morshed on 20/9/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedTabSub1 : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    
    UITableView *commentTableView;
    UIBarButtonItem *commentButton;
    
}

@property(nonatomic,retain) IBOutlet UITableView *commentTableView;
@property(nonatomic,retain) IBOutlet UIBarButtonItem *commentButton;

-(IBAction)showCommentScreen:(id)sender;

@end
