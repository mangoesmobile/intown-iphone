//
//  CityListTableController.m
//  inTown
//
//  Created by Mahbub Morshed on 17/8/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import "CityListTableController.h"
#import "JSON.h"

@implementation CityListTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        cityTitles = [[NSMutableArray alloc] init];
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cityTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cachedCell"];
    
    NSLog(@"I am Inside");
    
    if (cell == nil)
        cell = [[[UITableViewCell alloc] 
                 initWithFrame:CGRectZero reuseIdentifier:@"cachedCell"] autorelease];
    
    cell.textLabel.text = [cityTitles objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:15.0];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.shadowColor=[UIColor blackColor];
    cell.textLabel.highlightedTextColor=[UIColor blackColor];
    
    
    return cell;
}

-(void)clearList{
    [cityTitles removeAllObjects];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //When a certain row from the table is selected =>hide the searchbox =>save the selected city 
    
    //Hides the search box
    // searchTextField.hidden = YES;
    
    //get the current selected city
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cityCurrent = cell.textLabel.text;
    
    //save the current selected city 
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:cityCurrent forKey:@"currentCity"];
    
    
    //Getting the current selected image name
    NSString *myString = [prefs stringForKey:@"image"];
    
    //creating imageName.png string
    NSString *imageName= [NSString stringWithFormat:@"color%d%@",myString];
    
    //Logging the image name 
    NSLog(@"%@", imageName);
    
    //Storing cityName=> imageName.png
    [prefs setObject:cityCurrent forKey:imageName];
    
    //Log the current selected city and color
    NSLog(@"%@", cityCurrent);
    NSLog(@"%@", imageName);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
    // Store incoming data into a string
	NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"Inside the cityListTableController");
    // Create a dictionary from the JSON string
	//NSDictionary *results = [jsonString JSONValue];
    //NSLog(@"%@",results);
	
    NSArray *cities= [jsonString JSONValue];
    //NSLog(@"%@", cities);
    
    [jsonString release];
    
    // Build an array from the dictionary for easy access to each entry
	
    //NSArray *photos = [[results objectForKey:@"photos"] objectForKey:@"photo"];
    
    // Loop through each entry in the dictionary
	for (NSDictionary *city in cities)
    {
        // Get title of the image
		NSString *cityName = [city objectForKey:@"n"];
        NSString *countryName = [city objectForKey:@"c"];
        //NSString *cityName = [city objectForKey:@"n"];   
        
        NSString *cityCountry= [NSString stringWithFormat: @"%@, %@",cityName,countryName];
        //cityTitles= cityCountry;
        
        //NSLog(@"%@,%@",cityName,countryName);
        //NSLog(@"%@",cityCountry);
        
        [cityTitles addObject:cityCountry];
        NSLog(@"%@", cityCountry);
        //[self reloadData];
        // [theTableView reloadData];
        // [anotherTableView reloadData];
        
        // Save the title to the photo titles array
		//[photoTitles addObject:(title.length > 0 ? title : @"Untitled")];
        /*
         // Build the URL to where the image is stored (see the Flickr API)
         // In the format http://farmX.static.flickr.com/server/id/secret
         // Notice the "_s" which requests a "small" image 75 x 75 pixels
         NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_s.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
         debug(@"photoURLString: %@", photoURLString);
         // The performance (scrolling) of the table will be much better if we
         // build an array of the image data here, and then add this data as
         // the cell.image value (see cellForRowAtIndexPath:)
         [photoSmallImageData addObject:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoURLString]]];
         // Build and save the URL to the large image so we can zoom
         // in on the image if requested
         photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_m.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
         [photoURLsLargeImage addObject:[NSURL URLWithString:photoURLString]];        
         
         debug(@"photoURLsLareImage: %@\n\n", photoURLString); */
        
	}  
}

-(void)searchCity:(NSString *)text
{
    // Start the busy indicator
    // [activityIndicator startAnimating];
    
    // Build the string to call the Flickr API
	NSString *urlString = [NSString stringWithFormat:@"http://50.18.187.128/intown/city_lookup.php?q=%@", text];
    
    // Create NSURL string from formatted string
	NSURL *url = [NSURL URLWithString:urlString];
    
    // Setup and start async download
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection release];
    [request release];
    //NSLog(@"%@", urlString);
    //NSLog(@"Json request sent");
    
    //[self reloadData];
}



@end
