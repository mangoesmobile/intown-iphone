//
//  CityListTableController.h
//  inTown
//
//  Created by Mahbub Morshed on 17/8/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityListTableController : UITableViewController{
    NSMutableArray  *cityTitles;  //Titles of cities
}


-(void)searchCity:(NSString *)text;
-(void)clearList;
@end