//
//  ApplicationTabBarController.h
//  inTown
//
//  Created by Mahbub Morshed Prottoy on 6/8/11.
//  Copyright 2011 Mahbub Morshed Prottoy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarViewController.h"
#import "TownsSub1.h"

@interface ApplicationTabBarController : UITabBarController {
    
}

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;

@end
