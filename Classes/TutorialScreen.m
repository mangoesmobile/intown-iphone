//
//  TutorialScreen.m
//  inTown
//
//  Created by ManGoes Mobile 2 on 12/10/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import "TutorialScreen.h"

@implementation TutorialScreen
@synthesize button;
@synthesize facebook;
@synthesize opening_slide_board;
@synthesize slide_scroll;
@synthesize pageControl;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    UIImageView *image_slide_1 = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 233)]autorelease]; //create ImageView 
    UIImageView *image_slide_2 = [[[UIImageView alloc] initWithFrame:CGRectMake(321, 0, 320, 233)]autorelease]; //create ImageView 
    UIImageView *image_slide_3 = [[[UIImageView alloc] initWithFrame:CGRectMake(641, 0, 320, 233)]autorelease]; //create ImageView 
    UIImageView *image_slide_4 = [[[UIImageView alloc] initWithFrame:CGRectMake(961, 0, 320, 233)]autorelease]; //create ImageView 
    
    image_slide_4.image = [UIImage imageNamed:@"opening_slide_1.png"];
    image_slide_3.image = [UIImage imageNamed:@"opening_slide_2.png"];
    image_slide_2.image = [UIImage imageNamed:@"opening_slide_3.png"];
    image_slide_1.image = [UIImage imageNamed:@"opening_slide_4.png"];

        
    slide_scroll.contentSize = CGSizeMake(1280, 233);
    
    [slide_scroll addSubview:image_slide_1];
    [slide_scroll addSubview:image_slide_2];
    [slide_scroll addSubview:image_slide_3];
    [slide_scroll addSubview:image_slide_4];

    // Do any additional setup after loading the view from its nib.
}


//October 12th

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [facebook handleOpenURL:url]; 
}


//October 12th
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)closeMe:(id)sender{
    
    
    [self.view removeFromSuperview];
    
    /*
    //October 12th
    //Facebook 
    facebook = [[Facebook alloc] initWithAppId:@"194350703950433"];
    
    //Access Token, Expiration Date Key
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    
    // Facebook Permissions
    NSArray *permissions = [NSArray arrayWithObjects:@"user_location", @"friends_location", @"publish_stream", @"offline_access", @"email", @"read_friendlists", nil];
    
    if (![facebook isSessionValid]) {        
        [facebook authorize:permissions delegate:self];
    }
    
    NSLog(@"%@",facebook.accessToken);
    NSLog(@"The ExpirationDate is- %@",facebook.expirationDate);
    
    //Get the User Facebook User_ID in here
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"id", @"fields",nil];
    
    [facebook requestWithGraphPath:@"me"
                         andParams:params
                     andHttpMethod:@"GET"
                       andDelegate:self];
    
    //[facebook requestWithGraphPath:@"me?fields=id" andDelegate:self];*/
    
    //October 12th
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = scrollView.bounds.size.width;
    NSInteger pageNumber = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = pageNumber;
}
@end
