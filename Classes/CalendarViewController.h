//
//  CalendarViewController.h
//  inTown
//
//  Created by Mahbub Morshed Prottoy on 6/8/11.
//  Copyright 2011 Mahbub Morshed Prottoy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKCalendarMonthView.h"


//Facebook Connect
#import "FBConnect.h"

#import "AUITableViewController.h"
#import "CityListTableController.h"


#import "ApplicationTabBarController.h"
#import "MobileCoreServices/MobileCoreServices.h"

#import "TownsSub1.h"



@interface CalendarViewController : UIViewController <TKCalendarMonthViewDelegate,TKCalendarMonthViewDataSource, UIScrollViewDelegate,UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, FBSessionDelegate, FBRequestDelegate, UIGestureRecognizerDelegate> {
    
    //color list to show
    NSMutableArray *colorList;
    
    //City Search 
    UITextField *searchTextField;//The Search TextField for city Search

    
    UITableView     *cityListTableview;//Table That will show the city names
    CityListTableController *cityController;//This will hold the delegates and datasources for the thetableview 
    
    UITableView     *colorCityListTableView;//Table That will show the city names with colors
    AUITableViewController *nextt;
    
    NSMutableArray  *cityTitles;  //Titles of cities
    
    //These colors will be displayed to the popup
    NSMutableArray *cityColors;  //List of colors to show
    
	TKCalendarMonthView *calendar;//Instance of Tapku Calendar
    
    //This is where the marks will be stored
    NSMutableArray *data;
    //This is where the colors will be stored
    NSMutableArray *colors;
    
    UIImageView *popUpBackGroundImage;
    
    Facebook *facebook;
    
    int insert_id;
    
    //SEPTEMBER 14TH
    NSMutableArray *cityData ;
    NSMutableArray *colorData;
    NSMutableArray *colorToAdd;
    
    //November 16th
    NSMutableArray *cancellingDates;
    
    //Eight Buttons for eight colors in the popup
    UIButton *violetButton;
    UIButton *indigoButton;
    UIButton *blueButton;
    UIButton *greenButton;
    UIButton *yellowButton;
    UIButton *orangeButton;
    UIButton *redButton;
    UIButton *eighthButton;
    
    // Eight images for Eight Buttons
    UIImage *violetImage;
    UIImage *indigoImage;
    UIImage *blueImage;
    UIImage *greenImage;
    UIImage *yellowImage;
    UIImage *orangeImage;
    UIImage *redImage;
    UIImage *eighthImage;
    
    //Plus Button
    UIButton *addButton;
    
    //Cancel Button
    UIButton *cancelButton;
    
    //Done Button
    UIButton *doneButton;
    
    
}


@property (retain,nonatomic) NSMutableArray *data;

@property(nonatomic,retain) NSString *username;
@property(nonatomic,assign) BOOL post;
@property(nonatomic,retain) Facebook *facebook;
@property(nonatomic, retain) UITableView *cityListTableview;
@property(nonatomic,retain) UITableView     *colorCityListTableView;
@property(nonatomic,retain) NSMutableArray *colorList;
@property(nonatomic,retain) NSMutableArray *colorData;

@property(nonatomic,retain) UIButton *addButton;
@property(nonatomic,retain) UIButton *doneButton;
@property(nonatomic,retain) UIButton *cancelButton;


@property(nonatomic, retain) UIButton *color1;
@property(nonatomic, retain) UIButton *color2;
@property(nonatomic, retain) UIButton *color3;
@property(nonatomic, retain) UIButton *color4;
@property(nonatomic, retain) UIButton *color7;
@property(nonatomic, retain) UIButton *color8;
@property(nonatomic, retain) UIButton *color9;
@property(nonatomic, retain) UIButton *color10;

@property(nonatomic, retain) UIImage *violetImage;
@property(nonatomic, retain) UIImage *indigoImage;
@property(nonatomic, retain) UIImage *blueImage;
@property(nonatomic, retain) UIImage *greenImage;
@property(nonatomic, retain) UIImage *yellowImage;
@property(nonatomic, retain) UIImage *orangeImage;
@property(nonatomic, retain) UIImage *redImage;
@property(nonatomic, retain) UIImage *eighthImage;

//16th November
@property(nonatomic,retain) NSMutableArray *cancellingDates;

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
- (void)searchCity:(NSString *)text;
- (void)changeToPlus;
-(void)closeWithOutSaving;
-(void)changeToSelected;


@end
