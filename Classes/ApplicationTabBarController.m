//
//  ApplicationTabBarController.m
//  inTown
//
//  Created by Mahbub Morshed Prottoy on 6/8/11.
//  Copyright 2011 Mahbub Morshed Prottoy All rights reserved.
//

#import "ApplicationTabBarController.h"
#import "TownViewController.h"
#import "TownTab.h"
#import "FeedTab.h"
#import "FriendTab.h"
#import "DealsTab.h"


@implementation ApplicationTabBarController

#pragma mark -
#pragma mark View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		NSMutableArray *localControllers = [[NSMutableArray alloc] initWithCapacity:5];
		
        //FeedTab controller
        UINavigationController *fifthNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController3 = [[FeedTab alloc] initWithNibName:nil bundle:nil];
		[dummyViewController3.view setBackgroundColor:[UIColor blackColor]];
		[fifthNavigationController pushViewController:dummyViewController3 animated:NO];
		[[fifthNavigationController tabBarItem] setTitle:@"feed"];
        //fifthNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Feed.png"];
        fifthNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Tab_1.png"];
		[localControllers addObject:fifthNavigationController];
		[dummyViewController3 release];
        //[fifthNavigationController setTitle: @"Feed"];
		[fifthNavigationController release];
		
		// TownTab Contoller
		UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController = [[TownTab alloc] initWithNibName:nil bundle:nil];
		[dummyViewController.view setBackgroundColor:[UIColor blackColor]];
		[secondNavigationController pushViewController:dummyViewController animated:NO];
		[[secondNavigationController tabBarItem] setTitle:@"towns"];
        //secondNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Towns.png"];
        secondNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Tab_2.png"];
		[localControllers addObject:secondNavigationController];
		[dummyViewController release];
		[secondNavigationController release];
        
        // Create first navigation controller, add view holding calendar, 
        // assign title to navigation controller, add to local controllers array
		UINavigationController *firstNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		CalendarViewController *calendarViewController = [[CalendarViewController alloc] initWithNibName:nil bundle:nil];
		[firstNavigationController pushViewController:calendarViewController animated:NO];
		[[firstNavigationController tabBarItem] setTitle:@""];
        firstNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Calendar.png"];
		[localControllers addObject:firstNavigationController];
		[calendarViewController release];
		[firstNavigationController release];
        //firstNavigationController.delegate= self;
        
        // FriendTab Controller
		UINavigationController *thirdNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController1 = [[FriendTab alloc] initWithNibName:nil bundle:nil];
		[dummyViewController1.view setBackgroundColor:[UIColor blackColor]];
		[thirdNavigationController pushViewController:dummyViewController1 animated:NO];
		[[thirdNavigationController tabBarItem] setTitle:@"friends"];
        //thirdNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Friends.png"];
        thirdNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Tab_3.png"];
		[localControllers addObject:thirdNavigationController];
		[dummyViewController1 release];
		[thirdNavigationController release];
        
        
        
        
        // This is just a filler navigation controller for demo purposes
		UINavigationController *fourthNavigationController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
		UIViewController *dummyViewController2 = [[DealsTab alloc] initWithNibName:nil bundle:nil];
		[dummyViewController2.view setBackgroundColor:[UIColor blackColor]];
		[fourthNavigationController pushViewController:dummyViewController2 animated:NO];
		[[fourthNavigationController tabBarItem] setTitle:@"deals"];
        //fourthNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Deals.png"];
        fourthNavigationController.tabBarItem.image = [UIImage imageNamed:@"Icon_Tab_4.png"];
		[localControllers addObject:fourthNavigationController];
		[dummyViewController2 release];
		[fourthNavigationController release];
        
        
        
		// Add controllers to tab bar
		[self setViewControllers:localControllers animated:YES];
        //[self setRootViewController:firstNavigationController];
        
        [self setSelectedIndex:2];
        self.hidesBottomBarWhenPushed= YES;
		[localControllers release];
        
        [self addCenterButtonWithImage:[UIImage imageNamed:@"calendarButton.png"] highlightImage:nil];
        //[TownTab release];
        
        
    }
    return self;
}

#pragma mark -
#pragma mark Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
}

- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController respondsToSelector:@selector(willAppearIn:)])
        [viewController performSelector:@selector(willAppearIn:) withObject:navController];
}



// Create a custom UIButton and add it to the center of our tab bar
-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(selectCalendar) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        button.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        button.center = center;
    }
    
    [self.view addSubview:button];
}

-(void)selectCalendar{
    [self setSelectedIndex:2];
}

@end
