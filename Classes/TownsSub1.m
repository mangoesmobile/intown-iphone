//
//  TownsSub1.m
//  inTown
//
//  Created by ManGoes Mobile 2 on 15/9/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import "TownsSub1.h"

@implementation TownsSub1

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Cooper Black" size:35];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"Towns", @"");
    [label sizeToFit];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Post" style: UIBarButtonItemStylePlain target:self action:@selector(nextView) ]autorelease]; 
    
     self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)openFacebookPost{
    //TODO: Open post to facebook 
}

- (void)nextView{
    
    UIViewController *targetViewController = [[[TownsSub2 alloc]init ] autorelease]; 
    //[[self navigationController] pushViewController:targetViewController animated:YES];
    
    [targetViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentModalViewController:targetViewController animated:YES];
    
}

@end
