//
//  TutorialScreen.h
//  inTown
//
//  Created by ManGoes Mobile 2 on 12/10/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>
//Facebook Connect
//Thanks To Nayon
#import "FBConnect.h"

@interface TutorialScreen : UIViewController<FBSessionDelegate, FBRequestDelegate,UIScrollViewDelegate>{
    IBOutlet UIButton *button;
    Facebook *facebook;
    IBOutlet UIImageView *opening_slide_board;
    IBOutlet UIScrollView *slide_scroll;
    IBOutlet UIPageControl *pageControl;
}

@property (nonatomic,retain) IBOutlet UIButton *button;
@property(nonatomic,retain) Facebook *facebook;
@property(nonatomic,retain) IBOutlet UIImageView *opening_slide_board;
@property(nonatomic,retain)     IBOutlet UIScrollView *slide_scroll;
@property(nonatomic,retain)     IBOutlet UIPageControl *pageControl;

-(IBAction)closeMe:(id)sender;

@end
