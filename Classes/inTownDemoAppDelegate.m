//
//  inTownDemoAppDelegate.m
//  inTown
//
//  Created by Mahbub Morshed Prottoy on 6/8/11.
//  Copyright 2011 Mahbub Morshed Prottoy All rights reserved.
//


#import "inTownDemoAppDelegate.h"



@implementation inTownDemoAppDelegate

@synthesize window;
#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions { 
	
    tabBarController = [[ApplicationTabBarController alloc] initWithNibName:nil bundle:nil];
    
    tutorial = [[TutorialScreen alloc] initWithNibName:@"TutorialScreen" bundle:nil];
    
        
	[self.window addSubview:tabBarController.view];

    [self.window addSubview:tutorial.view];
    
    [self.window makeKeyAndVisible];
    
    [NSUserDefaults resetStandardUserDefaults];
    
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];

    return YES;
}

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    
}

- (void)dealloc {
	[tabBarController release];
    [window release];
    [super dealloc];
}

//push notification

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}




@end
