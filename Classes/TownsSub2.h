//
//  TownsSub2.h
//  inTown
//
//  Created by ManGoes Mobile 2 on 15/9/11.
//  Copyright 2011 Developing in the Dark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TownsSub2 : UIViewController{

    IBOutlet UIButton *postButton;
}


@property(nonatomic,retain) IBOutlet UIButton *postButton;
-(IBAction)posted:(id)sender;
@end
