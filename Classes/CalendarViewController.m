//
//  CalendarViewController.m
//  inTown
//
//  Created by Mahbub Morshed Prottoy on 6/8/11.
//  Copyright 2011 Mahbub Morshed Prottoy All rights reserved.
//

#import "CalendarViewController.h"
#import "inTownDemoAppDelegate.h"
#import "JSON.h"
#import "AUITableViewController.h"
#import "Connection.h"

@implementation CalendarViewController

@synthesize username;
@synthesize post;
@synthesize facebook;


@synthesize data;
@synthesize cityListTableview;
@synthesize colorCityListTableView;
@synthesize colorList;
@synthesize colorData;
//16th November
@synthesize cancellingDates;

//Eight popup buttons
@synthesize color1;
@synthesize color2;
@synthesize color3;
@synthesize color4;
@synthesize color7;
@synthesize color8;
@synthesize color9;
@synthesize color10;

//Eight popup images
@synthesize violetImage;
@synthesize indigoImage;
@synthesize blueImage;
@synthesize greenImage;
@synthesize yellowImage;
@synthesize orangeImage;
@synthesize redImage;
@synthesize eighthImage;

//Buttons below the calendar
@synthesize addButton;
@synthesize doneButton;
@synthesize cancelButton;

static int calendarShadowOffset = (int)-20;

#pragma mark -
#pragma mark View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    //ColorList add eight 1
    // Which means Eight colors will be shown 
    
    colorList=[[NSMutableArray alloc] initWithObjects:@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",nil];
    
    //October 23rd
    //set all previous settings to 0
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"-1"  forKey:@"color1.png"];
    
    [prefs setObject:nil  forKey:@"color1.png"];
    [prefs setObject:nil  forKey:@"color2.png"];
    [prefs setObject:nil  forKey:@"color3.png"];
    [prefs setObject:nil  forKey:@"color4.png"];
    [prefs setObject:nil  forKey:@"color5.png"];
    [prefs setObject:nil  forKey:@"color6.png"];
    [prefs setObject:nil  forKey:@"color7.png"];
    [prefs setObject:nil  forKey:@"color8.png"];
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization.
		calendar = 	[[TKCalendarMonthView alloc] init];
		calendar.delegate = self;
		calendar.dataSource = self;
        
        //CityTitle from JSON
        cityTitles = [[NSMutableArray alloc] init];
        
        //Data that will be converted into marks
        data = [[NSMutableArray alloc] init];
        cancellingDates = [[NSMutableArray alloc] init];
        
        //Colors Relating to city
        colors = [[NSMutableArray alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_textChanged:) name:UITextFieldTextDidChangeNotification object:self];
    }
    return self;
}

- (void)loadView {
	
    self.view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, calendar.frame.size.width, calendar.frame.size.height)] autorelease];
	self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
    //self.view.backgroundColor = [UIColor grayColor];
    self.view.backgroundColor=[UIColor whiteColor];
    
    
	// Add top left menu button to toggle calendar
	UIImage *menuButtonImage = [UIImage imageNamed:@"btn-calendar.png"];
	UIImage *menuButtonImageHighlighted = [UIImage imageNamed:@"btn-calendar-highlighted.png"];	
	UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[menuButton setImage:menuButtonImage forState:UIControlStateNormal];
	[menuButton setImage:menuButtonImageHighlighted forState:UIControlStateHighlighted];	
    menuButton.frame = CGRectMake(0, 0, menuButtonImage.size.width, menuButtonImage.size.height);
	UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
	[menuButton addTarget:self action:@selector(toggleCalendar) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = menuBarButton;
	[menuBarButton release];
    
    //This is the plus button
    addButton = [[UIButton alloc] initWithFrame:CGRectMake(258, 370, 32.0, 50.0)];
	addButton.backgroundColor = [UIColor clearColor];
    UIImage *btnImageAdd = [UIImage imageNamed:@"plus_button.png"];
    [addButton setImage:btnImageAdd forState:UIControlStateNormal];
	addButton.titleLabel.font = [UIFont systemFontOfSize:12];
	addButton.titleLabel.textColor = [UIColor whiteColor];
	[addButton setTitle:@"" forState:UIControlStateNormal];
	[addButton addTarget:self action:@selector(changeToPlus) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:addButton];
	
    //calendar.frame = CGRectMake(0, 0, calendar.frame.size.width/2, calendar.frame.size.height/2);
	//Add Calendar to just off the top of the screen so it can later slide down
	//calendar.frame = CGRectMake(0, -calendar.frame.size.height+calendarShadowOffset, calendar.frame.size.width, calendar.frame.size.height);
	// Ensure this is the last "addSubview" because the calendar must be the top most view layer	
	[self.view addSubview:calendar];
	[calendar reload];
    
}

- (void)viewDidLoad {
    
    //inTownDemoAppDelegate *appDelegate =(inTownDemoAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //This is hiding the top nevigation Bar
    self.navigationController.navigationBarHidden = YES;
    
    
    //Facebook is moved to Tutorial Screen
    
    //Ocotober 24th
    //Facebook 
    facebook = [[Facebook alloc] initWithAppId:@"194350703950433"];
    
    //Access Token, Expiration Date Key
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    
    // Facebook Permissions
    NSArray *permissions = [NSArray arrayWithObjects:@"user_location", @"friends_location", @"publish_stream", @"offline_access", @"email", @"read_friendlists", nil];
    
    if (![facebook isSessionValid]) {        
        [facebook authorize:permissions delegate:self localAppId:nil];
    }
    
    NSLog(@"%@",facebook.accessToken);
    NSLog(@"The ExpirationDate is- %@",facebook.expirationDate);
    
    //Get the User Facebook User_ID in here
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"id", @"fields",nil];
    
    [facebook requestWithGraphPath:@"me"
                         andParams:params
                     andHttpMethod:@"GET"
                       andDelegate:self];
    
    //[facebook requestWithGraphPath:@"me?fields=id" andDelegate:self];
    
    
    cityController =[[CityListTableController alloc]init];
    
    
    //SEPTEMBER 14TH
    colorData=[[ NSMutableArray alloc ]init];
    cityData=[[ NSMutableArray alloc ]init];
    
    for(int i=1; i<9; i++){
        //check whether there is a city name associated with the image name
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *myImageName= [NSString stringWithFormat:@"color%d%@", i ,@".png" ];
        
        // getting an NSString
        NSString *myCityName = [prefs stringForKey:myImageName];
        
        if(myCityName != nil){
            [colorData addObject: myImageName];    
            [cityData addObject:  myCityName];
        }
        
    }
    //SEPTEMBER 14TH
    
    [super viewDidLoad];
    
    //13th november
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *midasTouchDates= [[NSMutableDictionary alloc]init]; 
    NSData *dataMidasTouch= [NSKeyedArchiver archivedDataWithRootObject:midasTouchDates];
    [prefs setObject:dataMidasTouch forKey:@"midasTouchDictionary"];
    
    
    
    //16th November
    NSMutableDictionary *lastMidasTouchDates= [[NSMutableDictionary alloc]init]; 
    NSData *lastDataMidasTouch= [NSKeyedArchiver archivedDataWithRootObject:lastMidasTouchDates];
    [prefs setObject:lastDataMidasTouch forKey:@"lastMidasTouchDictionary"];
    
    
    //This is the done button
    doneButton = [[UIButton alloc] initWithFrame:CGRectMake(258, 370, 32.0, 50.0)];
	doneButton.backgroundColor = [UIColor clearColor];
    UIImage *btnImageDone = [UIImage imageNamed:@"done_button.png"];
    [doneButton setImage:btnImageDone forState:UIControlStateNormal];
	doneButton.titleLabel.font = [UIFont systemFontOfSize:12];
	doneButton.titleLabel.textColor = [UIColor whiteColor];
	[doneButton setTitle:@"" forState:UIControlStateNormal];
	[doneButton addTarget:self action:@selector(doneTagging) forControlEvents:UIControlEventTouchUpInside];
	
    
    
    //This is the cancel button
    cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(258, 325, 32.0, 50.0)];
	cancelButton.backgroundColor = [UIColor clearColor];
    UIImage *btnImageCancel = [UIImage imageNamed:@"cancel_button.png"];
    [cancelButton setImage:btnImageCancel forState:UIControlStateNormal];
	cancelButton.titleLabel.font = [UIFont systemFontOfSize:12];
	cancelButton.titleLabel.textColor = [UIColor whiteColor];
	[cancelButton setTitle:@"" forState:UIControlStateNormal];
	[cancelButton addTarget:self action:@selector(cancelTagging) forControlEvents:UIControlEventTouchUpInside];
	//[self.view addSubview:cancelButton];
    
    //October 10th
    [self changeToPlus];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [facebook handleOpenURL:url]; 
}

- (void)request:(FBRequest *)request didLoad:(id)result{
    
    [cityTitles removeAllObjects];
    
    NSLog(@"%@",result);
    
    NSString *userid= nil; 
    //NSMutableData *receivedData;
    
    if ([result isKindOfClass:[NSArray class]]) {
        result = [result objectAtIndex:0];
        NSLog(@"%@",result);
    }
    
    if ([result objectForKey:@"id"]) {
        userid=[result objectForKey:@"id"];
        
    }
    
    if([result isKindOfClass:[NSDictionary class]]){
        NSLog(@"I am here");
        
        NSArray *resultData = [result objectForKey:@"data"];
        
        //for (NSUInteger i=0; i<[resultData count] && i < 5; i++) {
        for(NSDictionary *item in resultData){
            NSLog(@"%@",[item objectForKey:@"name"]);
            [cityTitles addObject:[item objectForKey:@"name"]];
        }
        
    }
    
    [cityListTableview removeFromSuperview];
    
    cityListTableview = [[UITableView alloc] initWithFrame:CGRectMake(51, 42, 175, 123) style:UITableViewStylePlain];
    [cityListTableview setDelegate:self];
    [cityListTableview setDataSource:self];
    [cityListTableview setRowHeight:30];
    [cityListTableview setBackgroundColor:[UIColor clearColor]];
    [cityListTableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cityListTableview.showsVerticalScrollIndicator= NO;
    [cityListTableview setAllowsSelection:YES];
    
    [cityListTableview reloadData];
    [popUpBackGroundImage addSubview:cityListTableview];
    
    
    [cityListTableview reloadData];
    [colorCityListTableView reloadData];
    
    NSLog(@"My user id is- %@", userid);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:userid forKey:@"FBUserId"];
    
    
    NSString *access_token = [facebook accessToken];
    
    NSLog(@"%@", access_token);
}


- (void)fbDidLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    
    NSString *access_token = [facebook accessToken];
    NSLog(@"Access Token is-%@",access_token);
}

-(void) changeTo:(int )number{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    int myNumber= number;
    
    [prefs setInteger: myNumber forKey:@"image"];
    NSString *n= [NSString stringWithFormat:@"%d", number] ;
    
    
    [colorList replaceObjectAtIndex:0 withObject:n ];
}


-(void)changeTo0{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:0 forKey:@"image"];
}

-(void)changeTo1{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:1 forKey:@"image"];
    
    [self changeToSelected];
}

-(void)changeTo2{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:2 forKey:@"image"];
    
    [self changeToSelected];
}

-(void)changeTo3{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:3 forKey:@"image"];
    
    [self changeToSelected];
}

-(void)changeTo4{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:4 forKey:@"image"];
    
    [self changeToSelected];
}

-(void)changeTo5{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:5 forKey:@"image"];    
}

-(void)changeTo6{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:6 forKey:@"image"];
}

-(void)changeTo7{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:7 forKey:@"image"];
    
    [self changeToSelected];
}

-(void)changeTo8{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:8 forKey:@"image"];
    
    [self changeToSelected];
}
-(void)changeTo9{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:9 forKey:@"image"];
    
    [self changeToSelected];
}
-(void)changeTo10{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:10 forKey:@"image"];
    
    [self changeToSelected];
}
-(void)changeTo11{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:11 forKey:@"image"];
}

-(void)changeTo12{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:12 forKey:@"image"];
}

-(void) changeToSelected{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    int selectedImage=[prefs integerForKey:@"image"];
    
    UIImage *btnImageHigh = [UIImage imageNamed:@"button_glow.png"];
    UIImageView *buttonImageHighView= [[UIImageView alloc] initWithImage: btnImageHigh];
    buttonImageHighView.tag=1111111;
    
    if (selectedImage == 1) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        [color1 addSubview:buttonImageHighView];
        
    }
    
    else if (selectedImage == 2) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color2 addSubview:buttonImageHighView];
        
    }
    
    else if (selectedImage == 3) {
        
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color3 addSubview:buttonImageHighView];
    }
    
    else if (selectedImage == 4) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color4 addSubview:buttonImageHighView];
    }
    
    else if (selectedImage == 7) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color7 addSubview:buttonImageHighView];
    }
    
    else if (selectedImage == 8) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color8 addSubview:buttonImageHighView];
    }
    
    else if (selectedImage == 9) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color9 addSubview:buttonImageHighView];
    }
    
    else if (selectedImage == 10) {
        if([[colorList objectAtIndex:0]isEqual: @"1"]){
            [[color1 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:1]isEqual: @"1"]){
            [[color2 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:2]isEqual: @"1"]){
            [[color3 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:3]isEqual: @"1"]){
            [[color4 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:4]isEqual: @"1"]){
            [[color7 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:5]isEqual: @"1"]){
            [[color8 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:6]isEqual: @"1"]){
            [[color9 viewWithTag:1111111] removeFromSuperview];
        }
        if([[colorList objectAtIndex:7]isEqual: @"1"]){
            [[color10 viewWithTag:1111111] removeFromSuperview];
        }
        
        [color10 addSubview:buttonImageHighView];
        
    }
    
    [buttonImageHighView release];
}

-(void)changeToMinus{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger: -1 forKey:@"state"];
}

-(void)changeToPlus{
    
    NSLog(@"Inside changeToPlus");
    
    [addButton removeFromSuperview];
    
    [self.view addSubview:cancelButton];
    [self.view addSubview:doneButton];
    
    [colorCityListTableView reloadData];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger: 1 forKey:@"state"];
    [prefs setObject:@"public" forKey:@"r"];
    
    CGRect addCityFrame = CGRectMake(0, 0, 320, 480);
    UIView *addCityView = [[UIView alloc] initWithFrame:addCityFrame];
    addCityView.backgroundColor= [UIColor clearColor];
    addCityView.userInteractionEnabled=YES;
    [self.view addSubview: addCityView];
    [addCityView setTag: 9000];
    [addCityView release];
    
    CGRect backgroundrect = CGRectMake(40, 20, 240, 396);
    popUpBackGroundImage = [[UIImageView alloc] initWithFrame:backgroundrect];
    [popUpBackGroundImage setImage:[UIImage imageNamed:@"popup1.png"]];
    popUpBackGroundImage.alpha = 0.8;
    popUpBackGroundImage.userInteractionEnabled=YES;
    [addCityView addSubview:popUpBackGroundImage];
    [popUpBackGroundImage release];
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame: CGRectMake(70-60, 240 ,220 ,120 )];
    scroll.contentSize = CGSizeMake(220, 100);
    scroll.showsHorizontalScrollIndicator = YES;
    scroll.showsVerticalScrollIndicator= NO;
    [popUpBackGroundImage addSubview:scroll];
    //[addCityView addSubview:scroll];
    [scroll release];
    
    //UILabel *label0 = [ [UILabel alloc ] initWithFrame:CGRectMake(15, 20, 150.0, 43.0) ];
    UILabel *label0 = [ [UILabel alloc ] initWithFrame:CGRectMake(15-40, 0, 100.0, 43.0) ];
    label0.textAlignment =  UITextAlignmentCenter;
    label0.textColor = [UIColor whiteColor];
    label0.backgroundColor = [UIColor clearColor];
    label0.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(15.0)];
    label0.text = [NSString stringWithFormat: @"Town"];    
    [popUpBackGroundImage addSubview:label0];
    //[addCityView addSubview:label0];
    //[scroll addSubview:label0];
    
    [label0 release];
    //[scroll release];
    
    //Adding the searchTextField
    searchTextField = [[[UITextField alloc] initWithFrame:CGRectMake(51, 13, 175, 30)] retain];
    //[searchTextField UIReturnKeySend];
    [searchTextField setBorderStyle:UITextBorderStyleRoundedRect];
    searchTextField.placeholder = @"";
    searchTextField.returnKeyType = UIReturnKeyDone;
    searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    searchTextField.autocorrectionType= UITextAutocorrectionTypeNo;
    searchTextField.delegate = self;
    [searchTextField addTarget:self action:@selector(_textChanged) forControlEvents: UIControlEventEditingChanged];
    // [searchTextField disabledBackground];
    [popUpBackGroundImage addSubview:searchTextField];
    [searchTextField release];
    
    //This table is in the Popup
    cityListTableview = [[UITableView alloc] initWithFrame:CGRectMake(51, 42, 175, 123) style:UITableViewStylePlain];
    [cityListTableview setDelegate:self];
    [cityListTableview setDataSource:self];
    [cityListTableview setRowHeight:25];
    [cityListTableview setBackgroundColor:[UIColor clearColor]];
    [cityListTableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    //[theTableView allowsSelection: YES];
    cityListTableview.showsVerticalScrollIndicator= NO;
    [cityListTableview setAllowsSelection:YES];
    [popUpBackGroundImage addSubview:cityListTableview];
    
    
    //label 
    UILabel *label = [ [UILabel alloc ] initWithFrame:CGRectMake(25-40, 210, 100.0, 43.0) ];
    label.textAlignment =  UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(15.0)];
    label.text = [NSString stringWithFormat: @"Color"];    
    [popUpBackGroundImage addSubview:label];
    //[addCityView addSubview:label];
    
    [label release];
    
    
    // Add view button to add city
	UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(260, 5, 40, 40)];
	closeButton.backgroundColor = [UIColor clearColor];
    UIImage *btnImage4 = [UIImage imageNamed:@"cross.png"];
    [closeButton setImage:btnImage4 forState:UIControlStateNormal];
	closeButton.titleLabel.font = [UIFont systemFontOfSize:12];
	closeButton.titleLabel.textColor = [UIColor whiteColor];
	[closeButton setTitle:@"" forState:UIControlStateNormal];
	[closeButton addTarget:self action:@selector(closeWithOutSaving) forControlEvents:UIControlEventTouchUpInside];
	[addCityView addSubview:closeButton];
    //[addCityView addSubview:closeButton];
	[closeButton release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(80.0-40, 370-30, 160.0, 50)];
	button.backgroundColor = [UIColor clearColor];
    UIImage *btnImage5 = [UIImage imageNamed:@"button.png"];
    [button setImage:btnImage5 forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont systemFontOfSize:12];
	button.titleLabel.textColor = [UIColor whiteColor];
	[button setTitle:@"" forState:UIControlStateNormal];
	[button addTarget:self action:@selector(closeAddCityView) forControlEvents:UIControlEventTouchUpInside];
	[popUpBackGroundImage addSubview:button];
    [button setShowsTouchWhenHighlighted:YES];
    //[addCityView addSubview:button];
	[button release];
    
    UILabel *labelButton = [ [UILabel alloc ] initWithFrame:CGRectMake(80.0-40, 370-30, 160 , 40)];
    labelButton.textAlignment =  UITextAlignmentCenter;
    labelButton.textColor = [UIColor whiteColor];
    labelButton.backgroundColor = [UIColor clearColor];
    labelButton.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(25.0)];
    labelButton.text = [NSString stringWithFormat: @"TAG"];    
    [popUpBackGroundImage addSubview:labelButton];
    //[addCityView addSubview:labelButton];
    
    [labelButton release];
    
    int posX= 5;
    int posY= 0;
    
    if([colorList objectAtIndex:0]== @"1"){
        //Added
        // Add view button to change color to Red
        color1 = [[UIButton alloc] initWithFrame:CGRectMake(posX, posY, 50, 50)];
        [color1 setShowsTouchWhenHighlighted:YES];
        color1.backgroundColor = [UIColor clearColor];
        UIImage *btnImage1 = [UIImage imageNamed:@"color1.png"];
        [color1 setImage:btnImage1 forState:UIControlStateNormal];
        color1.titleLabel.font = [UIFont systemFontOfSize:12];
        color1.titleLabel.textColor = [UIColor redColor];
        [color1 setTitle:@"" forState:UIControlStateNormal];
        [color1 addTarget:self action:@selector(changeTo1) forControlEvents:UIControlEventTouchUpInside];
        [color1 setTag: 1000];
        //	[addCityView addSubview:redButton];
        [scroll addSubview: color1];
        [color1 release];
        posX= posX+50;
    }
    
    if([colorList objectAtIndex:1]== @"1"){
        //Added
        // Add view button to change color to yellow
        //UIButton *goldenButton = [[UIButton alloc] initWithFrame:CGRectMake(105, 190, 50, 50)];
        color2 = [[UIButton alloc] initWithFrame:CGRectMake(posX, posY, 50, 50)];
        [color2 setShowsTouchWhenHighlighted:YES];
        color2.backgroundColor = [UIColor clearColor];
        UIImage *btnImage = [UIImage imageNamed:@"color2.png"];
        //        UIImage *btnImageHigh = [UIImage imageNamed:@"color2_tapped.png"];
        [color2 setImage:btnImage forState:UIControlStateNormal];
        //        [orangeButton setImage:btnImageHigh forState:UIControlStateSelected];
        color2.titleLabel.font = [UIFont systemFontOfSize:12];
        color2.titleLabel.textColor = [UIColor whiteColor];
        [color2 setTitle:@"" forState:UIControlStateNormal];
        [color2 addTarget:self action:@selector(changeTo2) forControlEvents:UIControlEventTouchUpInside];
        //	[addCityView addSubview:goldenButton];
        [color1 setTag: 2000];        
        [scroll addSubview: color2];
        [color2 release];
        posX= posX+50;
    }
    
    
    if([colorList objectAtIndex:2]== @"1"){
        //Added
        // Add view button to change color to blue
        //UIButton *blueButton = [[UIButton alloc] initWithFrame:CGRectMake(155, 190, 50, 50)];
        color3 = [[UIButton alloc] initWithFrame:CGRectMake(posX, posY, 50, 50)];
        [color3 setShowsTouchWhenHighlighted:YES];
        color3.backgroundColor = [UIColor clearColor];
        UIImage *btnImage2 = [UIImage imageNamed:@"color3.png"];
        [color3 setImage:btnImage2 forState:UIControlStateNormal];
        color3.titleLabel.font = [UIFont systemFontOfSize:12];
        color3.titleLabel.textColor = [UIColor whiteColor];
        [color3 setTitle:@"" forState:UIControlStateNormal];
        [color3 addTarget:self action:@selector(changeTo3) forControlEvents:UIControlEventTouchUpInside];
        //	[addCityView addSubview:blueButton];
        [color3 setTag: 3000];
        
        [scroll addSubview: color3];
        [color3 release];
        posX= posX+50;
    }
    
    if([colorList objectAtIndex:3]== @"1"){
        //Added
        // Add view button to change Color to Green
        //UIButton *GreenButton = [[UIButton alloc] initWithFrame:CGRectMake(205, 190, 50, 50)];
        color4 = [[UIButton alloc] initWithFrame:CGRectMake(posX, posY, 50, 50)];
        [color4 setShowsTouchWhenHighlighted:YES];
        color4.backgroundColor = [UIColor clearColor];
        UIImage *btnImage0 = [UIImage imageNamed:@"color4.png"];
        [color4 setImage:btnImage0 forState:UIControlStateNormal];
        color4.titleLabel.font = [UIFont systemFontOfSize:12];
        color4.titleLabel.textColor = [UIColor whiteColor];
        [color4 setTitle:@"" forState:UIControlStateNormal];
        [color4 addTarget:self action:@selector(changeTo4) forControlEvents:UIControlEventTouchUpInside];
        //[addCityView addSubview:GreenButton];
        [color4 setTag: 4000];
        [scroll addSubview: color4];
        [color4 release];
        // posX= posX+50;
    }
    
    
    //Not Added
    UIButton *color5 = [[UIButton alloc] initWithFrame:CGRectMake(205, 0, 50, 50)];
    
	color5.backgroundColor = [UIColor clearColor];
    UIImage *btnImageYellow = [UIImage imageNamed:@"color5.png"];
    [color5 setImage:btnImageYellow forState:UIControlStateNormal];
	color5.titleLabel.font = [UIFont systemFontOfSize:12];
	color5.titleLabel.textColor = [UIColor whiteColor];
	[color5 setTitle:@"" forState:UIControlStateNormal];
	[color5 addTarget:self action:@selector(changeTo5) forControlEvents:UIControlEventTouchUpInside];
    //[addCityView addSubview:blueButton];
    //[scroll addSubview: color5];
	[color5 release];
    
    //Not Added
    UIButton *color6 = [[UIButton alloc] initWithFrame:CGRectMake(255, 0, 50, 50)];
	color6.backgroundColor = [UIColor clearColor];
    UIImage *btnImage6= [UIImage imageNamed:@"color6.png"];
    [color6 setImage:btnImage6 forState:UIControlStateNormal];
	color6.titleLabel.font = [UIFont systemFontOfSize:12];
	color6.titleLabel.textColor = [UIColor whiteColor];
	[color6 setTitle:@"" forState:UIControlStateNormal];
	[color6 addTarget:self action:@selector(changeTo6) forControlEvents:UIControlEventTouchUpInside];
    //	[addCityView addSubview:blueButton];
    //[scroll addSubview: color6];
	[color6 release];
    
    posX = 5;
    posY = 50;
    
    if([colorList objectAtIndex:4]== @"1"){
        //Added
        color7 = [[UIButton alloc] initWithFrame:CGRectMake(posX,posY, 50, 50)];
        [color7 setShowsTouchWhenHighlighted:YES];
        color7.backgroundColor = [UIColor clearColor];
        UIImage *btnImage7= [UIImage imageNamed:@"color7.png"];
        [color7 setImage:btnImage7 forState:UIControlStateNormal];
        color7.titleLabel.font = [UIFont systemFontOfSize:12];
        color7.titleLabel.textColor = [UIColor whiteColor];
        [color7 setTitle:@"" forState:UIControlStateNormal];
        [color7 addTarget:self action:@selector(changeTo7) forControlEvents:UIControlEventTouchUpInside];
        //	[addCityView addSubview:blueButton];
        [color7 setTag: 7000];
        
        [scroll addSubview: color7];
        [color7 release];
        posX=posX +50;
    }
    
    if([colorList objectAtIndex:5]== @"1"){
        //Added
        color8 = [[UIButton alloc] initWithFrame:CGRectMake(posX,posY, 50, 50)];
        [color8 setShowsTouchWhenHighlighted:YES];
        color8.backgroundColor = [UIColor clearColor];
        UIImage *btnImage8= [UIImage imageNamed:@"color8.png"];
        [color8 setImage:btnImage8 forState:UIControlStateNormal];
        color8.titleLabel.font = [UIFont systemFontOfSize:12];
        color8.titleLabel.textColor = [UIColor whiteColor];
        [color8 setTitle:@"" forState:UIControlStateNormal];
        [color8 addTarget:self action:@selector(changeTo8) forControlEvents:UIControlEventTouchUpInside];
        //	[addCityView addSubview:blueButton];
        [color8 setTag: 8000];
        [scroll addSubview: color8];
        [color8 release];
        posX=posX +50;
    }
    
    if([colorList objectAtIndex:6]== @"1"){
        //Added
        color9 = [[UIButton alloc] initWithFrame:CGRectMake(posX,posY, 50, 50)];
        [color9 setShowsTouchWhenHighlighted:YES];
        color9.backgroundColor = [UIColor clearColor];
        UIImage *btnImage9= [UIImage imageNamed:@"color9.png"];
        [color9 setImage:btnImage9 forState:UIControlStateNormal];
        color9.titleLabel.font = [UIFont systemFontOfSize:12];
        color9.titleLabel.textColor = [UIColor whiteColor];
        [color9 setTitle:@"" forState:UIControlStateNormal];
        [color9 addTarget:self action:@selector(changeTo9) forControlEvents:UIControlEventTouchUpInside];
        //[addCityView addSubview:blueButton];
        
        [color9 setTag: 9000];
        [scroll addSubview: color9];
        [color9 release];
        posX=posX +50;
    }
    
    
    if([colorList objectAtIndex:7]== @"1"){
        //Added
        color10 = [[UIButton alloc] initWithFrame:CGRectMake(posX,posY, 50, 50)];
        [color10 setShowsTouchWhenHighlighted:YES];
        color10.backgroundColor = [UIColor clearColor];
        UIImage *btnImage10= [UIImage imageNamed:@"color10.png"];
        [color10 setImage:btnImage10 forState:UIControlStateNormal];
        color10.titleLabel.font = [UIFont systemFontOfSize:12];
        color10.titleLabel.textColor = [UIColor whiteColor];
        [color10 setTitle:@"" forState:UIControlStateNormal];
        [color10 addTarget:self action:@selector(changeTo10) forControlEvents:UIControlEventTouchUpInside];
        //	[addCityView addSubview:blueButton];
        
        [color10 setTag: 10000];
        [scroll addSubview: color10];
        [color10 release];
        //posX=posX +50;
    }
    
    //posX=nil;
    //Not Added
    UIButton *color11 = [[UIButton alloc] initWithFrame:CGRectMake(205,  50, 50, 50)];
	color11.backgroundColor = [UIColor clearColor];
    UIImage *btnImage11= [UIImage imageNamed:@"color11.png"];
    [color11 setImage:btnImage11 forState:UIControlStateNormal];
	color11.titleLabel.font = [UIFont systemFontOfSize:12];
	color11.titleLabel.textColor = [UIColor whiteColor];
	[color11 setTitle:@"" forState:UIControlStateNormal];
	[color11 addTarget:self action:@selector(changeTo11) forControlEvents:UIControlEventTouchUpInside];
    //	[addCityView addSubview:blueButton];
    //[scroll addSubview: color11];
	[color11 release];
    //posX=posX +50;
    
    //Not Added
    UIButton *color12 = [[UIButton alloc] initWithFrame:CGRectMake(255,  50, 50, 50)];
	color12.backgroundColor = [UIColor clearColor];
    UIImage *btnImage12= [UIImage imageNamed:@"color12.png"];
    [color12 setImage:btnImage12 forState:UIControlStateNormal];
	color12.titleLabel.font = [UIFont systemFontOfSize:12];
	color12.titleLabel.textColor = [UIColor whiteColor];
	[color12 setTitle:@"" forState:UIControlStateNormal];
	//[color12 addTarget:self action:@selector(changeTo12) forControlEvents:UIControlEventTouchUpInside];
    //[addCityView addSubview:blueButton];
    // [scroll addSubview: color12];
	[color12 release];
    
    
    UILabel *label1 = [ [UILabel alloc ] initWithFrame:CGRectMake(25-45, 320-150, 150.0, 43.0) ];
    label1.textAlignment =  UITextAlignmentCenter;
    label1.textColor = [UIColor whiteColor];
    label1.backgroundColor = [UIColor clearColor];
    label1.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(15.0)];
    label1.text = [NSString stringWithFormat: @"Public Trip"];
    [popUpBackGroundImage addSubview:label1];
    //[addCityView addSubview:label1];
    [label1 release];
    
    UISwitch  *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(150-40, 330-150, 150.0, 43.0)];
    [onoff addTarget: self action: @selector(flip:) forControlEvents: UIControlEventValueChanged];
    [onoff setOn:YES animated:NO];
    // Set the desired frame location of onoff here
    [popUpBackGroundImage addSubview: onoff];
    //[addCityView addSubview: onoff];
    [onoff release];
}

//This is called when the cancel button is pressed
-(void)cancelTagging{
    
    //Remove the done button
    [doneButton removeFromSuperview];
    
    //Remove the cancel button
    [cancelButton removeFromSuperview];
    
    //Add the plus button
    [self.view addSubview:addButton ];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];    
    
    //Allow Tag
    //Set allow tag to NO (0)
    [prefs setInteger: 0 forKey:@"allow_tag"];
    
    
    for(int i=0; i<[cancellingDates count]; i++){
        
        if([data containsObject: [cancellingDates objectAtIndex:i]]){
            [data removeObject:[cancellingDates objectAtIndex:i]];
        }
    }
    
    for(int i=0; i< [data count]; i++){
        NSLog(@"%@", [data objectAtIndex:i]);
    }
    
    [cancellingDates removeAllObjects];
    [calendar reload];
    
    //TODO: Don't save anything 
}

//This is called when the done button is pressed
-(void)doneTagging{
    
    //Remove the doneButton
    [doneButton removeFromSuperview];
    
    //Remove the cancelButton
    [cancelButton removeFromSuperview];
    
    //Add the plus button to the view
    [self.view addSubview:addButton ];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];    
    
    //Set allow tag to NO (0)
    [prefs setInteger: 0 forKey:@"allow_tag"];
    
    [calendar reload];
    
    //TODO: Send the tagged dates to the server
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    if(tableView== self.cityListTableview){
        //October 25th
        return [cityTitles count];
    }
    else if(tableView== self.colorCityListTableView){
        return [colorData count];
    }
    else
        
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    UITableViewCell *cell = 
    [tableView dequeueReusableCellWithIdentifier:@"cachedCell"];
    NSLog(@"Inside table");
    
    NSLog(@"%@",cityTitles);
    
    if (cell == nil)
        //   cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"cachedCell"] autorelease];
        
    {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"cachedCell"] autorelease];
        
		UILongPressGestureRecognizer *longPressGesture =
        [[[UILongPressGestureRecognizer alloc]
          initWithTarget:self action:@selector(handleLongPress:)] autorelease];
		[cell addGestureRecognizer:longPressGesture];
    }
    
    if (tableView== self.cityListTableview) {
        
        cell.textLabel.text = [cityTitles objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.shadowColor=[UIColor blackColor];
        cell.textLabel.highlightedTextColor=[UIColor blackColor];
        
        
    }
    
    else if (tableView ==self.colorCityListTableView){
        //@try {
        
        NSString *name=[colorData objectAtIndex:indexPath.row];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *myString = [prefs stringForKey:name];
        
        cell.imageView.image=[UIImage imageNamed:name];
        //cell.imageView.image.size= 15.0;
        
        if(myString!= nil){
            
            cell.textLabel.text = myString;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
            cell.textLabel.textColor=[UIColor blackColor];
            cell.textLabel.shadowColor=[UIColor clearColor];
            cell.textLabel.highlightedTextColor=[UIColor blackColor];
        }
        else{
            cell.textLabel.text = @"";
            cell.textLabel.font = [UIFont systemFontOfSize:15.0];
            cell.textLabel.textColor=[UIColor redColor];
            cell.textLabel.shadowColor=[UIColor clearColor];
            cell.textLabel.highlightedTextColor=[UIColor blackColor];        
        }
        return cell;
        
        //    }
        //@catch (NSException *exception) {
        //  NSLog(@"Exception Caught");
        //}
        //@finally {
        //  return cell;
        //}
        
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //When a certain row from the table is selected =>hide the keyboard =>save the selected city 
    [searchTextField resignFirstResponder];
    
    //Hides the search box
    //searchTextField.hidden = YES;
    if(tableView== self.cityListTableview){
        
        //get the current selected city
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *cityCurrent = cell.textLabel.text;
        
        //save the current selected city 
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:cityCurrent forKey:@"currentCity"];
        
        
        //October 25th
    }
    else if(tableView== self.colorCityListTableView){
        
        // Navigation logic may go here. Create and push another view controller.
        UIViewController *targetViewController = [[[TownsSub1 alloc]init ] autorelease]; 
        [[self navigationController] pushViewController:targetViewController animated:YES];
    }
}



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)dataCon 
{
    // Store incoming data into a string
	NSString *jsonString = [[NSString alloc] initWithData:dataCon encoding:NSUTF8StringEncoding];
    
    // Create a dictionary from the JSON string
	//NSDictionary *results = [jsonString JSONValue];
    //NSLog(@"%@",results);
	
    NSArray *cities= [jsonString JSONValue];
    //NSLog(@"%@", cities);
    
    [jsonString release];
    
    //Build an array from the dictionary for easy access to each entry
	
    //NSArray *photos = [[results objectForKey:@"photos"] objectForKey:@"photo"];
    
    // Loop through each entry in the dictionary...
	for (NSDictionary *item in cities)
    {
        // Get title of the image
		NSString *cityName = [item objectForKey:@"name"];
        NSString *countryName = [item objectForKey:@"subtext"];
        //NSString *cityName = [city objectForKey:@"n"];   
        
        NSString *cityCountry= [NSString stringWithFormat: @"%@, %@",cityName,countryName];
        //cityTitles= cityCountry;
        
        //NSLog(@"%@,%@",cityName,countryName);
        NSLog(@"%@",cityCountry);
        
        [cityTitles addObject:cityCountry];
        
        [cityListTableview removeFromSuperview];
        
        cityListTableview = [[UITableView alloc] initWithFrame:CGRectMake(51, 42, 175, 123) style:UITableViewStylePlain];
        [cityListTableview setDelegate:self];
        [cityListTableview setDataSource:self];
        [cityListTableview setRowHeight:30];
        [cityListTableview setBackgroundColor:[UIColor clearColor]];
        [cityListTableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        cityListTableview.showsVerticalScrollIndicator= NO;
        [cityListTableview setAllowsSelection:YES];
        
        [cityListTableview reloadData];
        [popUpBackGroundImage addSubview:cityListTableview];
        
        
        [cityListTableview reloadData];
        [colorCityListTableView reloadData];
        
    }  
}



-(void)searchCity:(NSString *)text
{
    // Start the busy indicator
    //[activityIndicator startAnimating];
    NSString *access_token = [facebook accessToken];
    
    // Build the string to call the API
	NSString *urlString = [NSString stringWithFormat:@"/search?q=%@&type=adcity&access_token=%@", text, access_token];
    NSLog(@"%@", urlString);
    
    [facebook requestWithGraphPath:urlString andDelegate:self];
    
    /*   October 25th
     
     // Create NSURL string from formatted string
     NSURL *url = [NSURL URLWithString:urlString];
     
     // Setup and start async download
     NSURLRequest *request = [[NSURLRequest alloc] initWithURL: url];
     NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
     [connection release];
     [request release];
     NSLog(@"%@", urlString);
     //NSLog(@"Json request sent");
     */
    [cityListTableview reloadData];
}



-(IBAction)flip:(id)sender {  
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *current= [prefs objectForKey:@"r"];
    
    if([current isEqualToString:@"public"]){
        [prefs setObject:@"private" forKey:@"r"];
    }
    else
        [prefs setObject:@"public" forKey:@"r"];
    
    NSLog(@"flipped");
}

-(void)closeAddCityView{
    
    //When the TagCity button is pressed 
    
    //=> get the current city 
    //=> get the current image 
    //=> store currentcity<-currentimage.png
    //=> Set allow tagging to YES
    //=> Close the popup
    
    UIView *city = [self.view viewWithTag:9000];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //Set allow tag to YES(1)
    [prefs setInteger: 1 forKey:@"allow_tag"];
    
    //Getting the current city
    NSString *myCity = [prefs stringForKey:@"currentCity"];
    
    //Getting the current image
    NSInteger myImageInt = [prefs integerForKey:@"image"];
    
    if(myImageInt <4){
        
        NSString *save= [NSString stringWithFormat:@"%d",myImageInt-1];
        [colorList replaceObjectAtIndex:0 withObject: save];
    }
    
    if(myImageInt >4){
        
        NSString *save= [NSString stringWithFormat:@"%d",myImageInt-3];
        [colorList replaceObjectAtIndex:0 withObject: save];
    }
    
    
    //Getting the current image name
    NSString *myImageName= [[NSString alloc] initWithFormat:@"color%d%@",myImageInt,@".png"];
    
    NSLog(@"%@",myImageName);
    NSLog(@"%@", myCity);
    
    //=>store currentcity <- currentimage.png 
    [prefs setObject:myCity forKey:myImageName];
    
    [myImageName release];
    
    //city.hidden = YES;
    
    while([[city subviews] count]) {
        [[[city subviews] objectAtIndex:0] removeFromSuperview];
    }
    
    [city removeFromSuperview];
    
    //[anotherTableView reloadData];
    [colorCityListTableView removeFromSuperview];
    nextt = [[AUITableViewController alloc]init];
    
    colorCityListTableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 320, 250, 100) style:UITableViewStylePlain];
    [colorCityListTableView setDelegate: self];
    [colorCityListTableView setDataSource:self];
    [colorCityListTableView setRowHeight:30];
    [colorCityListTableView setBackgroundColor:[UIColor clearColor]];
    [colorCityListTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    colorCityListTableView.separatorColor= [UIColor whiteColor];
    
    //[anotherTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [colorCityListTableView setAllowsSelection:YES];
    
    colorCityListTableView.showsVerticalScrollIndicator= NO;
    [self.view addSubview:colorCityListTableView];
    
    //september30 Dynamic loading
    
    colorData=[[ NSMutableArray alloc ]init];
    cityData=[[ NSMutableArray alloc ]init];
    
    for(int i=1; i<9; i++){
        //check whether there is a city name associated with the image name
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *myImageName= [NSString stringWithFormat:@"color%d%@", i ,@".png" ];
        
        // getting an NSString
        NSString *myCityName = [prefs stringForKey:myImageName];
        
        if(myCityName != nil){
            [colorData addObject: myImageName];    
            [cityData addObject:  myCityName];
        }
    }
    
    
    NSString *alert_message= [NSString stringWithFormat:@"you have selected to tag with %@", myCity];
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Notification"
                                                      message: alert_message
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
    [message show];
    
    //September 30 Dynamic Loading
    
}

//Close PopUp and don't Tag with colors
-(void)closeWithOutSaving{
    //Select the popup
    UIView *city = [self.view viewWithTag:9000];
    [city removeFromSuperview];
    
    [cancelButton removeFromSuperview];
    [doneButton removeFromSuperview];
    
    [self.view addSubview:addButton];
}

// Show/Hide the calendar by sliding it down/up from the top of the device.
- (void)toggleCalendar {
	// If calendar is off the screen, show it, else hide it (both with animations)
	if (calendar.frame.origin.y == -calendar.frame.size.height+calendarShadowOffset) {
		// Show
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:.75];
		calendar.frame = CGRectMake(0, 0, calendar.frame.size.width, calendar.frame.size.height);
		[UIView commitAnimations];
	} else {
		// Hide
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:.75];
		calendar.frame = CGRectMake(0, -calendar.frame.size.height+calendarShadowOffset, calendar.frame.size.width, calendar.frame.size.height);		
		[UIView commitAnimations];
	}	
}

#pragma mark -
#pragma mark TKCalendarMonthViewDelegate methods

- (void)calendarMonthView:(TKCalendarMonthView *)monthView didSelectDate:(NSDate *)d {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:d];
    
    [dateFormat release];
    
    NSString *storeString= [dateString stringByAppendingString: @" 00:00:00 +0000"];
    NSLog(@"Date selected is- %@",storeString);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *myString = [prefs stringForKey:@"image"];
    
    int colorValue= [prefs integerForKey:@"image"];
    
    //Find whether tagging is allowed
    NSInteger tagging_allowed= [prefs integerForKey:@"allow_tag"];
    
    if (colorValue == -1){
        [self changeToPlus];    
    }
    
    else{
        
        
        //If tagging is allowed than store the selected date
        if(tagging_allowed ==1){
            
            //If data already contains the date            
            if([data containsObject:storeString]){
                if ([[prefs objectForKey:storeString]isEqual: myString]) {
                    NSLog(@"Already tagged with similar Color");
                    
                    [data removeObject:storeString];
                    [prefs setObject: @"0" forKey:storeString];
                }
                NSLog(@"Date is already Tagged");
            }
            //If data does not conatins the date add the date 
            else{
                //Mark stored
                [data addObject:storeString];
                [cancellingDates addObject:storeString];
            }
        }  
//        if ([[prefs objectForKey:storeString]isEqual: myString]) {
//            NSLog(@"Already tagged With Similar Color Tag removed");
//            [prefs setObject: @"0" forKey:storeString];
//        }
//        else {
            [prefs setObject:myString forKey:storeString];
//        }
        
    }
    
}

- (void)calendarMonthView:(TKCalendarMonthView *)monthView monthDidChange:(NSDate *)d {
    
	NSLog(@"calendarMonthView monthDidChange");	
}

#pragma mark -
#pragma mark TKCalendarMonthViewDataSource methods

- (NSArray*)calendarMonthView:(TKCalendarMonthView *)monthView marksFromDate:(NSDate *)startDate toDate:(NSDate *)lastDate {	
    
    [colorCityListTableView removeFromSuperview];
    
	NSLog(@"calendarMonthView marksFromDate toDate");	
	NSLog(@"Make sure to update 'data' variable to pull from CoreData, website, User Defaults, or some other source.");
	
    // When testing initially you will have to update the dates in this array so they are visible at the
	// time frame you are testing the code.
    
    //data= [NSMutableArray arrayWithCapacity:0];
    //data = [NSMutableArray arrayWithObjects:
    //      @"2011-01-01 00:00:00 +0000", @"2011-01-09 00:00:00 +0000", @"2011-01-22 00:00:00 +0000",
    //    @"2011-01-10 00:00:00 +0000", @"2011-01-11 00:00:00 +0000", @"2011-01-31 00:00:00 +0000",
    //  nil]; 
	
    
	//[data addObject: @"2011-01-01 00:00:00 +0000"];
    
	// Initialise empty marks array, this will be populated with TRUE/FALSE in order for each day a marker should be placed on.
	NSMutableArray *marks = [NSMutableArray array];
    
    // Initialise empty marks color array, this will be populated with numbers in order for each day a color marker should be placed on.
    NSMutableArray *marksColor = [NSMutableArray array];	
    
	// Initialise calendar to current type and set the timezone to never have daylight saving
	NSCalendar *cal = [NSCalendar currentCalendar];
	[cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	// Construct DateComponents based on startDate so the iterating date can be created.
	// Its massively important to do this assigning via the NSCalendar and NSDateComponents because of daylight saving has been removed 
	// with the timezone that was set above. If you just used "startDate" directly (ie, NSDate *date = startDate;) as the first 
	// iterating date then times would go up and down based on daylight savings.
	NSDateComponents *comp = [cal components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | 
											  NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) 
									fromDate:startDate];
	NSDate *d = [cal dateFromComponents:comp];
	
	// Init offset components to increment days in the loop by one each time
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setDay:1];	
	
	
	// for each date between start date and end date check if they exist in the data array
	while (YES) {
		// Is the date beyond the last date? If so, exit the loop.
		// NSOrderedDescending = the left value is greater than the right
		if ([d compare:lastDate] == NSOrderedDescending) {
			break;
		}
		
		// If the date is in the data array, add it to the marks array, else don't
		if ([data containsObject:[d description]]) {
            NSLog(@"This is d description %@",[d description]);
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *myString = [prefs stringForKey:[d description]];
            
            if (myString != nil){
                [marks addObject:myString];
                [marksColor addObject: @"1"];            
            }
			//[marks addObject:[NSNumber numberWithBool:YES]];
            
		} else {
            
			//[marks addObject:[NSNumber numberWithBool:NO]];                        
            [marks addObject:@"0"];                        
            [marksColor addObject: @"0"];
		}
		
        
		// Increment day using offset components (ie, 1 day in this instance)
		d = [cal dateByAddingComponents:offsetComponents toDate:d options:0];
	}
	
	[offsetComponents release];
    
    nextt = [[AUITableViewController alloc]init];
    
    colorCityListTableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 320, 250, 100) style:UITableViewStylePlain];
    [colorCityListTableView setDelegate: self];
    [colorCityListTableView setDataSource:self];
    [colorCityListTableView setRowHeight:25];
    [colorCityListTableView setBackgroundColor:[UIColor clearColor]];
    [colorCityListTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    colorCityListTableView.separatorColor= [UIColor whiteColor];
    
    //[anotherTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [colorCityListTableView setAllowsSelection:YES];
    
    colorCityListTableView.showsVerticalScrollIndicator= NO;
    [self.view addSubview:colorCityListTableView];
    
	return [NSArray arrayWithArray:marks];
}


#pragma mark -
#pragma mark -
#pragma mark Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    NSLog(@"deallocating objects");
    
    [cityTitles release];
    [nextt release];
    [cityController release];
    
    [username release];
    [colorList release];
    
    [addButton release];
    [doneButton release];
    [cancelButton release];
	
    
    //[insert_id release];
    [data release];
    [cancellingDates release];
    [super dealloc];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [textField resignFirstResponder];
    
    // Remove any content from a previous search
    [cityTitles removeAllObjects];
    
    [cityController clearList];
    
    NSString *cityString = [searchTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    //[self searchCity: searchTextField.text];
    [self searchCity: cityString];
    
    [cityListTableview reloadData];
    
    
    return YES;
}

- (void)_textChanged {
    NSLog(@"%@", searchTextField.text);
    
    [cityTitles removeAllObjects];
    
    if (searchTextField.text.length >= 1) {
        [cityController clearList];
        
        NSString *cityString = [searchTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        [self searchCity:cityString];
        //sleep(1000);
        
        [cityListTableview reloadData];
        NSLog(@"tableview reloaded"); 
    }
    
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.colorCityListTableView];
    
    NSIndexPath *indexPath = [self.colorCityListTableView indexPathForRowAtPoint:p];
    if (indexPath == nil)
        NSLog(@"long press on table view but not on a row");
    else{
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
        {
            [self changeToPlus];
            NSLog(@"long press on table view at row %d", indexPath.row);
            
        }
        
    }
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:NO];
}

@end