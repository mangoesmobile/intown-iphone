//
//  Comment.h
//  inTown
//
//  Created by Mahbub Morshed on 10/9/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Comment : UIViewController<UITextViewDelegate>{
    UITextView *comment;
}

@property(nonatomic,retain) IBOutlet UITextView *comment;

@end
