//
//  TownViewController.m
//  inTown
//
//  Created by Mahbub Morshed on 20/8/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import "TownViewController.h"


@implementation TownViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        //[super setRowHeight:200];
        //[theTableView setBackgroundColor:[UIColor clearColor]];
        
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[super setEditing:YES];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    //cell.textLabel.text=@"Name of Town";
    //cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:12.0];;
    
    // Configure the cell...
    
    CGRect Label1Frame = CGRectMake(10, 10, 290, 25);
    UILabel *lbl1;
    
    lbl1 = [[UILabel alloc] initWithFrame:Label1Frame];
    lbl1.tag = 1;
    lbl1.font= [UIFont fontWithName:@"Helvetica" size:15];
    lbl1.textColor= [UIColor lightGrayColor];
    
    lbl1.text=@"SAN FRANCISCO, CA";
    [lbl1 setBackgroundColor:[UIColor blackColor]];
	[cell.contentView addSubview:lbl1];
	[lbl1 release];
    
    CGRect Label2Frame = CGRectMake(10, 30, 290, 25);
    UILabel *lbl2;
    
    lbl2 = [[UILabel alloc] initWithFrame:Label2Frame];
	lbl2.tag = 1;
    lbl2.font= [UIFont fontWithName:@"Helvetica" size:12];
    lbl2.textColor= [UIColor lightGrayColor];
    
    lbl2.text=@"Jan 1- Jan 3";
    [lbl2 setBackgroundColor:[UIColor blackColor]];
	[cell.contentView addSubview:lbl2];
	[lbl2 release];
    
    
    UIScrollView *scroll = [[[UIScrollView alloc] initWithFrame: CGRectMake(35, 50, 250, 100)] autorelease];
    scroll.contentSize = CGSizeMake(500, 50);
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator= NO;
    scroll.pagingEnabled=YES;
    [scroll setTag:100];
    
    //[cell.contentView removeChildWithTag: 100];
    [cell.contentView addSubview:scroll];
    
    
    
    //[addCityView addSubview:scroll];
    //[scroll addSubview:lblTemp];
    
    for (int i=0 ; i<10 ; i++) {
        
        NSString *stringName=@"profilepic.png";
        
        if(i%2 ==0){
         stringName=@"profilepic1.png";
        }
        
        
        UIImageView *btnImage1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:stringName] ];
        btnImage1.frame= CGRectMake((i*50) , 0, 50, 50);
        scroll.backgroundColor= [UIColor clearColor];
        
        [scroll addSubview:btnImage1];
        [btnImage1 release];
    }
    
    
    
    //[color1 release];
    
//    [scroll release];
    
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 
*/
/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject: 2] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }*/
 

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    UIViewController *targetViewController = [[[TownsSub1 alloc]init ] autorelease]; 
    //[self.tableView removeFromSuperview];
    [[self navigationController] pushViewController:targetViewController animated:YES];
     
}

/*
 - (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
 
 CGRect CellFrame = CGRectMake(0, 0, 300, 60);
 CGRect Label1Frame = CGRectMake(10, 10, 290, 25);
 //CGRect Label2Frame = CGRectMake(10, 33, 290, 25);
 UILabel *lblTemp;
 
 UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
 
 //Initialize Label with tag 1.
 lblTemp = [[UILabel alloc] initWithFrame:Label1Frame];
 lblTemp.tag = 1;
 lblTemp.text="Name of Town";
 [cell.contentView addSubview:lblTemp];
 [lblTemp release];
 
 /*
 //Initialize Label with tag 2.
 lblTemp = [[UILabel alloc] initWithFrame:Label2Frame];
 lblTemp.tag = 2;
 lblTemp.font = [UIFont boldSystemFontOfSize:12];
 lblTemp.textColor = [UIColor lightGrayColor];
 [cell.contentView addSubview:lblTemp];
 [lblTemp release];
 
 */  

/*   
 UIScrollView *scroll = [[UIScrollView alloc] initWithFrame: Label2Frame];
 scroll.contentSize = CGSizeMake(220, 100);
 scroll.showsHorizontalScrollIndicator = YES;
 scroll.showsVerticalScrollIndicator= NO;
 [cell.contentView addSubview:scroll];
 //[addCityView addSubview:scroll];
 [scroll addSubview:lblTemp];
 
 [scroll release];
 */ 
/*return cell;
 }*/

@end
