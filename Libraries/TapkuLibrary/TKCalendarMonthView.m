//
//  TKCalendarMonthView.m
//  Created by Devin Ross on 6/10/10.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

#import "TKCalendarMonthView.h"
#import "NSDate+TKCategory.h"
#import "TKGlobal.h"
#import "UIImage+TKCategory.h"

//#define kCalendImagesPath @"TapkuLibrary.bundle/Images/calendar/"

@interface NSDate (calendarcategory)

- (NSDate*) firstOfMonth;
- (NSDate*) nextMonth;
- (NSDate*) previousMonth;

@end

@implementation NSDate (calendarcategory)

- (NSDate*) firstOfMonth{
    
    TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    info.day = 1;
    info.minute = 0;
    info.second = 0;
    info.hour = 0;
    return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}
- (NSDate*) nextMonth{
    
    TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    info.month++;
    if(info.month>12){
        info.month = 1;
        info.year++;
    }
    info.minute = 0;
    info.second = 0;
    info.hour = 0;
    
    return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
}
- (NSDate*) previousMonth{
    
    
    TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    info.month--;
    if(info.month<1){
        info.month = 12;
        info.year--;
    }
    
    info.minute = 0;
    info.second = 0;
    info.hour = 0;
    return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
}

@end




@interface TKCalendarMonthTiles : UIView {
    
    id target;
    SEL action;
    
    int firstOfPrev,lastOfPrev;
    NSMutableArray *marks;
    
    int today;
    BOOL markWasOnToday;
    
    int selectedDay,selectedPortion;
    
    int firstWeekday, daysInMonth;
    UILabel *dot;
    UILabel *currentDay;
    UIImageView *selectedImageView;
    
    UIImage *tile;
    
    BOOL startOnSunday;
    NSDate *monthDate;
    
    UIImageView *myImage;
    
    //    NSMutableDictionary *midasTouchDates;
}

//@property (nonatomic,retain) NSMutableDictionary *midasTouchDates;
@property (readonly) NSDate *monthDate;

- (id) initWithMonth:(NSDate*)date marks:(NSArray*)marks startDayOnSunday:(BOOL)sunday;
- (void) setTarget:(id)target action:(SEL)action;

- (void) selectDay:(int)day;
- (NSDate*) dateSelected;

+ (NSArray*) rangeOfDatesInMonthGrid:(NSDate*)date startOnSunday:(BOOL)sunday;

@end

#define dotFontSize 18.0
#define dateFontSize 22.0

@interface TKCalendarMonthTiles (private)

@property (readonly) UIImageView *selectedImageView;
@property (readonly) UIImageView *myImage;
@property (readonly) UILabel *currentDay;
@property (readonly) UILabel *dot;
@end

@implementation TKCalendarMonthTiles
@synthesize monthDate;




+ (NSArray*) rangeOfDatesInMonthGrid:(NSDate*)date startOnSunday:(BOOL)sunday{
    
    
    NSDate *firstDate, *lastDate;
    
    
    TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    info.day = 1;
    info.hour = 0;
    info.minute = 0;
    info.second = 0;
    
    NSDate *currentMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    info = [currentMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    
    NSDate *previousMonth = [currentMonth previousMonth];
    NSDate *nextMonth = [currentMonth nextMonth];
    
    if(info.weekday > 1 && sunday){
        
        TKDateInformation info2 = [previousMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        int preDayCnt = [previousMonth daysBetweenDate:currentMonth];		
        info2.day = preDayCnt - info.weekday + 2;
        firstDate = [NSDate dateFromDateInformation:info2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        
    }else if(!sunday && info.weekday != 2){
        
        TKDateInformation info2 = [previousMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        int preDayCnt = [previousMonth daysBetweenDate:currentMonth];
        if(info.weekday==1){
            info2.day = preDayCnt - 5;
        }else{
            info2.day = preDayCnt - info.weekday + 3;
        }
        firstDate = [NSDate dateFromDateInformation:info2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
    }else{
        firstDate = currentMonth;
    }
    
    
    
    int daysInMonth = [currentMonth daysBetweenDate:nextMonth];		
    info.day = daysInMonth;
    NSDate *lastInMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    TKDateInformation lastDateInfo = [lastInMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    
    
    if(lastDateInfo.weekday < 7 && sunday){
        
        lastDateInfo.day = 7 - lastDateInfo.weekday;
        lastDateInfo.month++;
        lastDateInfo.weekday = 0;
        if(lastDateInfo.month>12){
            lastDateInfo.month = 1;
            lastDateInfo.year++;
        }
        lastDate = [NSDate dateFromDateInformation:lastDateInfo timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
    }else if(!sunday && lastDateInfo.weekday != 1){
        
        lastDateInfo.day = 8 - lastDateInfo.weekday;
        lastDateInfo.month++;
        if(lastDateInfo.month>12){ lastDateInfo.month = 1; lastDateInfo.year++; }
        
        lastDate = [NSDate dateFromDateInformation:lastDateInfo timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
    }else{
        lastDate = lastInMonth;
    }
    
    
    
    return [NSArray arrayWithObjects:firstDate,lastDate,nil];
}

- (id) initWithMonth:(NSDate*)date marks:(NSArray*)markArray startDayOnSunday:(BOOL)sunday{
    
    if(![super initWithFrame:CGRectZero]) return nil;
    
    firstOfPrev = -1;
    marks = [markArray retain];
    monthDate = [date retain];
    startOnSunday = sunday;
    
    TKDateInformation dateInfo = [monthDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    firstWeekday = dateInfo.weekday;
    
    
    NSDate *prev = [monthDate previousMonth];
    //NSDate *next = [monthDate nextMonth];
    
    daysInMonth = [[monthDate nextMonth] daysBetweenDate:monthDate];
    
    int row = (daysInMonth + dateInfo.weekday - 1);
    if(dateInfo.weekday==1&&!sunday) row = daysInMonth + 6;
    if(!sunday) row--;
    
    
    row = (row / 7) + ((row % 7 == 0) ? 0:1);
    float h = 44 * row;
    
    TKDateInformation todayInfo = [[NSDate date] dateInformation];
    today = dateInfo.month == todayInfo.month && dateInfo.year == todayInfo.year ? todayInfo.day : -5;
    
    int preDayCnt = [prev daysBetweenDate:monthDate];		
    if(firstWeekday>1 && sunday){
        firstOfPrev = preDayCnt - firstWeekday+2;
        lastOfPrev = preDayCnt;
    }else if(!sunday && firstWeekday != 2){
        
        if(firstWeekday ==1){
            firstOfPrev = preDayCnt - 5;
        }else{
            firstOfPrev = preDayCnt - firstWeekday+3;
        }
        lastOfPrev = preDayCnt;
        
    }
    
    // [self.selectedImageView.image=];
    
    self.frame = CGRectMake(0, 1, 320, h+1);
    
    [self.selectedImageView addSubview:self.currentDay];
    
    //Uncomment to Add dot
    //[self.selectedImageView addSubview:self.dot];
    
    
    self.multipleTouchEnabled = NO;
    
    return self;
}

- (void) dealloc {
    
    [currentDay release];
    [dot release];
    
    [selectedImageView release];
    
    [marks release];
    [monthDate release];
    [super dealloc];
}

- (void) setTarget:(id)t action:(SEL)a{
    
    target = t;
    action = a;
}


- (CGRect) rectForCellAtIndex:(int)index{
    
    int row = index / 7;
    int col = index % 7;
    
    return CGRectMake(col*46, row*44+6, 47, 45);
}


//This is where it needs to be changed
- (void) drawTileInRect:(CGRect)r day:(int)day mark:(NSString *)mark font:(UIFont*)f1 font2:(UIFont*)f2 {
    
    //NSLog(@"DrawTilein rect called with mark %@", mark);
    NSString *str = [NSString stringWithFormat:@"%d",day];
    
    
    //font at middle
    r.origin.y -= 6.5;
    
    if([mark isEqualToString:(@"0")]){
        
    }
    else{
        //r.size.height -= 8;
        NSString *colorString= [NSString stringWithFormat:@"color%@%@", mark, @".png"];
        //NSLog(@"Inside Draw Tile in rect. mark=1");
        //r.origin.y -= 6.5;
        [[UIImage imageWithContentsOfFile:TKBUNDLE(colorString)] drawInRect:r];        
    }
    
    r.origin.y += 6.5;
    
    r.size.height -= 2;
    [str drawInRect: r
           withFont: f1
      lineBreakMode: UILineBreakModeWordWrap 
          alignment: UITextAlignmentCenter];
    //October 16th
    r.origin.y += 6.5;// prevoiusly 6.5
}

//This is what drawing all the background images
- (void) drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    //tile = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile.png")];
    tile = [UIImage imageWithContentsOfFile:TKBUNDLE(@"Month Calendar Date Tile.png")];
    
    CGRect r = CGRectMake(0, 0, 46, 44);
    CGContextDrawTiledImage(context, r, tile.CGImage);
    
    
    if(today > 0){
        int pre = firstOfPrev > 0 ? lastOfPrev - firstOfPrev + 1 : 0;
        int index = today +  pre-1;
        CGRect r =[self rectForCellAtIndex:index];
        
        //October 16th
        r.origin.y -= 7;// This is the position of the today tile 
        
        //[[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Today Tile.png")] drawInRect:r];
        [[UIImage imageWithContentsOfFile:TKBUNDLE(@"Month Calendar Today Tile.png")] drawInRect:r];
    }
    
    
    int index = 0;
    
    UIFont *font = [UIFont boldSystemFontOfSize:dateFontSize];
    UIFont *font2 =[UIFont boldSystemFontOfSize:dotFontSize];
    UIColor *color = [UIColor grayColor];
    
    
    if(firstOfPrev>0){
        [color set];
        for(int i = firstOfPrev;i<= lastOfPrev;i++){
            r = [self rectForCellAtIndex:index];
            if ([marks count] > 0){
                [self drawTileInRect:r day:i mark:(NSString *)[marks objectAtIndex:index]  font:font font2:font2];
            }
            else
                [self drawTileInRect:r day:i mark:@"0" font:font font2:font2];
            index++;
        }
    }
    
    
    color = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
    [color set];
    for(int i=1; i <= daysInMonth; i++){
        
        r = [self rectForCellAtIndex:index];
        if(today == i) [[UIColor whiteColor] set];
        
        if ([marks count] > 0) {
            NSString *colorToDraw= (NSString *)[marks objectAtIndex:index] ;
            [self drawTileInRect:r day:i mark:colorToDraw  font:font font2:font2];
            
        }
        else
            [self drawTileInRect:r day:i mark:@"0" font:font font2:font2];
        if(today == i) [color set];
        index++;
    }
    
    [[UIColor grayColor] set];
    int i = 1;
    while(index % 7 != 0){
        r = [self rectForCellAtIndex:index] ;
        if ([marks count] > 0) {
            //[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
            [self drawTileInRect:r day:i mark:(NSString *)[marks objectAtIndex:index]  font:font font2:font2];
            //            NSLog(@"prottoy- %@",[marks objectAtIndex:index]);
        }
        else
            [self drawTileInRect:r day:i mark:@"0" font:font font2:font2];
        i++;
        index++;
    }
    
    
}

- (void) selectDay:(int)day{
    //NSLog(@"SelectDay");
    
    int pre = firstOfPrev < 0 ?  0 : lastOfPrev - firstOfPrev + 1;
    
    int tot = day + pre;
    int row = tot / 7;
    int column = (tot % 7)-1;
    
    selectedDay = day;
    selectedPortion = 1;
    
    
    if(day == today){
        self.currentDay.shadowOffset = CGSizeMake(0, 1);
        self.dot.shadowOffset = CGSizeMake(0, 1);
        self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"Month Calendar Today Selected Tile.png")];
        //self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"Golden.png")];
        markWasOnToday = YES;
    }else if(markWasOnToday){
        self.dot.shadowOffset = CGSizeMake(0, -1);
        self.currentDay.shadowOffset = CGSizeMake(0, -1);
        
        self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected.png")];
        markWasOnToday = NO;
    }
    
    [self addSubview:self.selectedImageView];
    
    self.currentDay.text = [NSString stringWithFormat:@"%d",day];
    
    if ([marks count] > 0) {
        
        if([[marks objectAtIndex: row * 7 + column ] boolValue]){
            
            [self.selectedImageView addSubview:self.dot];
        }else{
            [self.dot removeFromSuperview];
        }
        
        
    }else{
        [self.dot removeFromSuperview];
    }
    
    if(column < 0){
        column = 6;
        row--;
    }
    
    CGRect r = self.selectedImageView.frame;
    r.origin.x = (column*46);
    r.origin.y = (row*44)-1;
    self.selectedImageView.frame = r;
}

- (NSDate*) dateSelected{
    if(selectedDay < 1 || selectedPortion != 1) return nil;
    
    TKDateInformation info = [monthDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    info.hour = 0;
    info.minute = 0;
    info.second = 0;
    info.day = selectedDay;
    NSDate *d = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    
    NSNumber *numberOne = [[NSNumber alloc] initWithInt: selectedDay];
    
    
    [numberOne release];
    return d;
    
}



- (void) reactToTouch:(UITouch*)touch down:(BOOL)down{
    
    //[self reloadInputViews];
    //Print the Selected Date
    //NSLog(@"today is - %@", [self dateSelected]);
    
    //change the selected image color
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSInteger myInt = [prefs integerForKey:@"image"];
    
    NSData *dataLoad= [prefs objectForKey:@"midasTouchDictionary"];
    //16th November
    NSData *lastDataLoad= [prefs objectForKey:@"lastMidasTouchDictionary"];
    
    NSDictionary *midasTouchDates=[[NSDictionary alloc]init];
    
    //16th November
    NSDictionary *lastMidasTouchDates=[[NSDictionary alloc]init];    
    
    midasTouchDates  = [NSKeyedUnarchiver unarchiveObjectWithData: dataLoad];
    //16th November
    lastMidasTouchDates  = [NSKeyedUnarchiver unarchiveObjectWithData: lastDataLoad];
    
    //NSArray *allDictionaryKeys=[midasTouchDates allKeys];
    //NSArray *allDictionaryValues=[midasTouchDates allValues];
    
    NSMutableDictionary *midasTouchMutableDates= [[NSMutableDictionary alloc] initWithDictionary:midasTouchDates];    
    //16th November
    NSMutableDictionary *lastMidasTouchMutableDates= [[NSMutableDictionary alloc] initWithDictionary:lastMidasTouchDates];  
    
    //midasTouchMutableDates= [NSMutableDictionary dictionaryWithObject:allDictionaryValues forKey:allDictionaryKeys];
    
    //Check Whether it is tagging mode or not
    NSInteger tagging_allowed= [prefs integerForKey:@"allow_tag"];
    
    NSString *colorString= @"color0.png";
    
    //NSString *colorString= [NSString stringWithFormat:@"color%d%@", myInt, @".png"];
    //if (tagging_allowed == 1) {
    
    if(myInt== 1){
        colorString=@"color1.png";
    }
    else if(myInt== 2){
        colorString=@"color2.png";
    }
    else if(myInt== 3){
        colorString=@"color3.png";
    }
    else if(myInt== 4){
        colorString=@"color4.png";
    }
    else if(myInt== 5){
        colorString=@"color5.png";
    }
    else if(myInt== 6){
        colorString=@"color6.png";
    }
    else if(myInt== 7){
        colorString=@"color7.png";        
    }
    else if(myInt== 8){
        colorString=@"color8.png";
    }
    else if(myInt== 9){
        colorString=@"color9.png";
    }
    else if(myInt== 10){
        colorString=@"color10.png";
    }
    else if(myInt== 11){
        colorString=@"color11.png";
    }
    else if(myInt== 12){
        colorString=@"color12.png";
    }
    else
        colorString=@"color0.png";
    
    //NSLog(@"This is month- %@", monthDate.monthString);
    //NSLog(@"This is year- %@", monthDate.yearString);
    
    //No need to touch this 
    CGPoint p = [touch locationInView:self];
    if(p.y > self.bounds.size.height || p.y < 0) return;
    
    int column = p.x / 46, row = p.y / 44;
    int day = 1, portion = 0;
    
    if(row == (int) (self.bounds.size.height / 44)) row --;
    
    int fir = firstWeekday - 1;
    if(!startOnSunday && fir == 0) fir = 7;
    if(!startOnSunday) fir--;
    
    
    if(row==0 && column < fir){
        day = firstOfPrev + column;
    }else{
        portion = 1;
        day = row * 7 + column  - firstWeekday+2;
        if(!startOnSunday) day++;
        if(!startOnSunday && fir==6) day -= 7;
    }
    if(portion > 0 && day > daysInMonth){
        portion = 2;
        day = day - daysInMonth;
    }
    
    
    if(portion != 1){
        markWasOnToday = YES;
    }else if(portion==1 && day == today){
        self.currentDay.shadowOffset = CGSizeMake(0, 1);
        self.dot.shadowOffset = CGSizeMake(0, 1);
        //self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Today Selected Tile.png")];
        
        markWasOnToday = YES;
    }else if(markWasOnToday){
        self.dot.shadowOffset = CGSizeMake(0, -1);
        self.currentDay.shadowOffset = CGSizeMake(0, -1);
        //self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected.png")];
        
        markWasOnToday = NO;
    }
    
    self.currentDay.text = [NSString stringWithFormat:@"%d",day];
    self.currentDay.textColor= [UIColor blackColor];
    [self.currentDay setTag:day];
    
    [self.selectedImageView addSubview:self.currentDay];
    self.currentDay.hidden =YES;
    
    
    int currentDate= day; 
    
    NSString *todayDMY= [NSString stringWithFormat:@"%@%d%@", monthDate.monthString, currentDate, monthDate.yearString];
    
    //15th november
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    //Key date string is the current selected date that will be used as a key
    NSString *keyDateString = [dateFormat stringFromDate:[self dateSelected]];
    
    [dateFormat release];
    
    //15th November
    NSString *stringToCompare= (NSString *)[midasTouchMutableDates objectForKey:todayDMY];
    
    self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"selected_image.png")];
    
    self.myImage.image = [UIImage imageWithContentsOfFile:TKBUNDLE(colorString)];
    
    [self addSubview:self.selectedImageView];
    
    if ([marks count] > 0) {
        if([[marks objectAtIndex: row * 7 + column] isEqualToString:@"0"]){
            //[self.selectedImageView addSubview:self.dot];
            [self.dot removeFromSuperview];
        }
        
    }else{
        [self.selectedImageView addSubview:self.dot];
    }
    
    CGRect r = self.selectedImageView.frame;
    r.origin.x = (column*46);
    r.origin.y = (row*44)-0.7;
    self.selectedImageView.frame = r;
    
    
    if(day == selectedDay && selectedPortion == portion) return;
    
    if(portion == 1){
        // If Date is not tagged already
        if([self viewWithTag:currentDate]==nil){
            //And if tagging is allowed
            if( tagging_allowed == 1){
                
                UIImageView *myImage1 = [[UIImageView alloc] initWithFrame:r];
                [myImage1 setImage:[UIImage imageNamed:colorString]];
                
                //[self addSubview:myImage1];
                [myImage1 setTag: currentDate];//the image is tagged with the date
                
                myImage1.opaque = NO; // explicitly opaque for performance
                
                //font at middle while tagging
                UILabel *dateLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(0, 2,45,33) ];
                dateLabel.textAlignment =  UITextAlignmentCenter;
                dateLabel.textColor = [UIColor blackColor];
                dateLabel.backgroundColor = [UIColor clearColor];
                dateLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(dateFontSize)];
                //[self addSubview:dateLabel];
                dateLabel.text = [NSString stringWithFormat: @"%d", currentDate];
                
                [myImage1 addSubview:dateLabel];
                [dateLabel release];
                
                [self addSubview:myImage1];
                
                [colorString release];
                [myImage1 release];
            }
        }
        
        //if date is Tagged Already
        else if([self viewWithTag:currentDate]!=nil){
            //If tagging is allowed
            if( tagging_allowed == 1){
                //If current color and the color user is tagging with is not the same override color
                if(![stringToCompare isEqual: colorString ]){
                    
                    self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"selected_image.png")];
                    
                    
                    //Removing the old Color
                    UIView *v = [self viewWithTag:currentDate];
                    [v removeFromSuperview]; 
            
                    //Color overriding
                    UIImageView *myImage1 = [[UIImageView alloc] initWithFrame:r];
                    [myImage1 setImage:[UIImage imageNamed:colorString]];
                    [myImage1 setTag: currentDate];
                    myImage1.opaque = NO; // explicitly opaque for performance
                    
                    //font at middle while tagging
                    
                    //UILabel *dateLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(0, 2,45, 25) ];
                    UILabel *dateLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(0, 2,45, 33) ];
                    dateLabel.textAlignment =  UITextAlignmentCenter;
                    dateLabel.textColor = [UIColor blackColor];
                    dateLabel.backgroundColor = [UIColor clearColor];
                    dateLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(dateFontSize)];
                    //[self addSubview:dateLabel];
                    dateLabel.text = [NSString stringWithFormat: @"%d", currentDate];
                    
                    [myImage1 addSubview:dateLabel];
                    
                    [midasTouchMutableDates setObject:colorString forKey:todayDMY];
    
                    //For Cancelling the last Update 
                    [lastMidasTouchMutableDates setObject:colorString forKey:todayDMY];
                    
                    //This is where it is saved
                    NSData *data= [NSKeyedArchiver archivedDataWithRootObject:midasTouchMutableDates];
                    [prefs setObject:data forKey:@"midasTouchDictionary"];
                    
                    //For Cancelling the last Update
                    [prefs setObject:data forKey:@"lastMidasTouchDictionary"];
                    
                    [dateLabel release];                    
                    
                    [self addSubview:myImage1];
                    [colorString release];
                    [myImage1 release];
                }
                
                
                // If current color and the color user is tagging with is same remove the color
                else{
                    
                    //Select with a transparent Image
                    self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"selected_image.png")];
                    //self.myImage.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"selected_image.png")];
                    
                    //Remove the current Color
                    UIView *v = [self viewWithTag:currentDate];
                    [v removeFromSuperview]; 
                    
                    
                    [midasTouchMutableDates setObject:@"nil" forKey:todayDMY];
                    [lastMidasTouchMutableDates setObject:@"nil" forKey:todayDMY];
            
                    NSData *data= [NSKeyedArchiver archivedDataWithRootObject:midasTouchMutableDates];
                    [prefs setObject:data forKey:@"midasTouchDictionary"];
                    [prefs setObject:data forKey:@"lastMidasTouchDictionary"];
                    
                }
            }
        }
        
        else {
            
            //NSLog(@"Now in else - %d",currentDate);
            self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"selected_image.png")];
            
            UIView *v = [self viewWithTag:currentDate];
            //v.hidden = YES;
            //v.alpha = 0;
            //[self bringSubviewToFront:v];
            [v removeFromSuperview];
            //[prefs setObject:nil forKey: keyDateString];
        }
        
        selectedDay = day;
        selectedPortion = portion;
        [target performSelector:action withObject:[NSArray arrayWithObject:[NSNumber numberWithInt:day]]];
        
    }
    else if(down){
        
        [target performSelector:action withObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:day],[NSNumber numberWithInt:portion],nil]];
        selectedDay = day;
        selectedPortion = portion;
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self reactToTouch:[touches anyObject] down:YES];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [self reactToTouch:[touches anyObject] down:NO];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self reactToTouch:[touches anyObject] down:NO];
}

- (UILabel *) currentDay{
    if(currentDay==nil){
        CGRect r = self.selectedImageView.bounds;
        r.origin.y -= 8.4;// previously it was 2
        currentDay = [[UILabel alloc] initWithFrame:r];
        currentDay.text = @"1";
        currentDay.textColor = [UIColor whiteColor];
        currentDay.backgroundColor = [UIColor clearColor];
        currentDay.font = [UIFont boldSystemFontOfSize:dateFontSize];
        currentDay.textAlignment = UITextAlignmentCenter;
        currentDay.shadowColor = [UIColor darkGrayColor];
        currentDay.shadowOffset = CGSizeMake(0, -1);
    }
    return currentDay;
}

//Not adding this dot in midasTouchTapku Calendar
- (UILabel *) dot{
    if(dot==nil){
        CGRect r = self.selectedImageView.bounds;
        r.origin.y += 29;// previously it was 29
        r.size.height -= 31; // previously it was 31
        dot = [[UILabel alloc] initWithFrame:r];
        
        dot.text = @"•";
        dot.textColor = [UIColor whiteColor];
        dot.backgroundColor = [UIColor clearColor];
        dot.font = [UIFont boldSystemFontOfSize:dotFontSize];
        dot.textAlignment = UITextAlignmentCenter;
        dot.shadowColor = [UIColor darkGrayColor];
        dot.shadowOffset = CGSizeMake(0, -1);
    }
    return dot;
}

- (UIImageView *) myImage{
    if(myImage==nil){
        CGRect r = self.selectedImageView.frame;
        //r.origin.y = -6.5;
        //r.size.height -= 31;
        //myImage = [[UILabel alloc] initWithFrame:r];
        
        myImage = [[UIImageView alloc] initWithFrame:r];
        [myImage setImage:[UIImage imageNamed: @"color0.png"]];
    }
    return myImage;
}


- (UIImageView *) selectedImageView{
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:0 forKey:@"userid"];
    NSInteger userid = [userDefaults integerForKey:@"userid"];

    
    if(selectedImageView==nil){
        if (userid!= 1) {
            
            //19th Nov
           // selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected"]];
            selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamedTK:@"selected_image.png"]];
        }
        else
        {
        }
        
        //selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected"]];
        selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"selected_image.png")]];
    }
    return selectedImageView;
}


@end



@interface TKCalendarMonthView (private)

@property (readonly) UIScrollView *tileBox;
@property (readonly) UIImageView *topBackground;
@property (readonly) UILabel *monthYear;
@property (readonly) UIButton *leftArrow;
@property (readonly) UIButton *rightArrow;
@property (readonly) UIImageView *shadow;

@end


@implementation TKCalendarMonthView
@synthesize delegate,dataSource;
@synthesize midasTouchDates;


- (id) init{
    self = [self initWithSundayAsFirst:YES];
    return self;
    leftMonthCount=0;
    rightMonthCount=0;
}
- (id) initWithSundayAsFirst:(BOOL)s{
    if (!(self = [super initWithFrame:CGRectZero])) return nil;
    self.backgroundColor = [UIColor grayColor];
    
    sunday = s;
    
    
    currentTile = [[[TKCalendarMonthTiles alloc] initWithMonth:[[NSDate date] firstOfMonth] marks:nil startDayOnSunday:sunday] autorelease];
    [currentTile setTarget:self action:@selector(tile:)];
    
    [currentTile setTarget:self action:@selector(tile:)];
    CGRect r = CGRectMake(0, 0, self.tileBox.bounds.size.width, self.tileBox.bounds.size.height + self.tileBox.frame.origin.y);
    
    
    self.frame = r;
    
    
    [currentTile retain];
    
    [self addSubview:self.topBackground];
    [self.tileBox addSubview:currentTile];
    [self addSubview:self.tileBox];
    
    NSDate *date = [NSDate date];
    self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[date monthString],[date yearString]];
    [self addSubview:self.monthYear];
    
    
    [self addSubview:self.leftArrow];
    [self addSubview:self.rightArrow];
    [self addSubview:self.shadow];
    self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"eee"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    
    TKDateInformation sund;
    sund.day = 5;
    sund.month = 12;
    sund.year = 2010;
    sund.hour = 0;
    sund.minute = 0;
    sund.second = 0;
    sund.weekday = 0;
    
    
    NSTimeZone *tz = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSString * sun = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    sund.day = 6;
    NSString *mon = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    sund.day = 7;
    NSString *tue = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    sund.day = 8;
    NSString *wed = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    sund.day = 9;
    NSString *thu = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    sund.day = 10;
    NSString *fri = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    sund.day = 11;
    NSString *sat = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
    
    [dateFormat release];
    
    
    
    NSArray *ar;
    if(sunday) ar = [NSArray arrayWithObjects:sun,mon,tue,wed,thu,fri,sat,nil];
    else ar = [NSArray arrayWithObjects:mon,tue,wed,thu,fri,sat,sun,nil];
    
    int i = 0;
    for(NSString *s in ar){
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(46 * i, 29, 46, 15)];
        [self addSubview:label];
        label.text = s;
        label.textAlignment = UITextAlignmentCenter;
        label.shadowColor = [UIColor whiteColor];
        label.shadowOffset = CGSizeMake(0, 1);
        label.font = [UIFont systemFontOfSize:11];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
        
        i++;
        [label release];
    }
    
    return self;
}
- (void) dealloc {
    [shadow release];
    [topBackground release];
    [leftArrow release];
    [monthYear release];
    [rightArrow release];
    [tileBox release];
    [currentTile release];
    [super dealloc];
}


- (NSDate*) dateForMonthChange:(UIView*)sender {
    BOOL isNext = (sender.tag == 1);
    NSDate *nextMonth = isNext ? [currentTile.monthDate nextMonth] : [currentTile.monthDate previousMonth];
    
    TKDateInformation nextInfo = [nextMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *localNextMonth = [NSDate dateFromDateInformation:nextInfo];
    
    return localNextMonth;
}

- (void) changeMonthAnimation:(UIView*)sender{
    
    BOOL isNext = (sender.tag == 1);
    
    //Show limited Number of months- Prottoy
    if(isNext){
        leftMonthCount++;
        rightMonthCount--;
    }
    else{
        rightMonthCount++;
        leftMonthCount--;
    }
    
    
    NSDate *nextMonth = isNext ? [currentTile.monthDate nextMonth] : [currentTile.monthDate previousMonth];
    
    TKDateInformation nextInfo = [nextMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *localNextMonth = [NSDate dateFromDateInformation:nextInfo];
    
    
    NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:nextMonth startOnSunday:sunday];
    NSArray *ar = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
    TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithMonth:nextMonth marks:ar startDayOnSunday:sunday];
    [newTile setTarget:self action:@selector(tile:)];
    
    
    
    int overlap =  0;
    
    if(isNext){
        overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : 44;
    }else{
        overlap = [currentTile.monthDate compare:[dates lastObject]] !=  NSOrderedDescending ? 44 : 0;
    }
    
    float y = isNext ? currentTile.bounds.size.height - overlap : newTile.bounds.size.height * -1 + overlap +2;
    
    newTile.frame = CGRectMake(0, y, newTile.frame.size.width, newTile.frame.size.height);
    newTile.alpha = 0;
    [self.tileBox addSubview:newTile];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    newTile.alpha = 1;
    
    [UIView commitAnimations];
    
    
    
    self.userInteractionEnabled = NO;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(animationEnded)];
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.4];
    
    
    
    if(isNext){
        
        currentTile.frame = CGRectMake(0, -1 * currentTile.bounds.size.height + overlap + 2, currentTile.frame.size.width, currentTile.frame.size.height);
        newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
        self.tileBox.frame = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
        
        self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
        
        
    }else{
        
        newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
        self.tileBox.frame = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
        currentTile.frame = CGRectMake(0,  newTile.frame.size.height - overlap, currentTile.frame.size.width, currentTile.frame.size.height);
        
        self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
        
    }
    
    
    [UIView commitAnimations];
    
    oldTile = currentTile;
    currentTile = newTile;
    
    
    //Show limited numbers of month
    if(leftMonthCount >=12){
        rightArrow.hidden=YES;
    }
    else
        rightArrow.hidden=NO;
    
    if(rightMonthCount >=2){
        leftArrow.hidden=YES;
    }
    else
        leftArrow.hidden=NO;

    
    monthYear.text = [NSString stringWithFormat:@"%@ %@",[localNextMonth monthString],[localNextMonth yearString]];
}
- (void) changeMonth:(UIButton *)sender{
    
    NSDate *newDate = [self dateForMonthChange:sender];
    if ([delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)] && ![delegate calendarMonthView:self monthShouldChange:newDate animated:YES] ) 
        return;
    
    
    if ([delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)] ) 
        [delegate calendarMonthView:self monthWillChange:newDate animated:YES];
    
    [self changeMonthAnimation:sender];
    if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
        [delegate calendarMonthView:self monthDidChange:currentTile.monthDate animated:YES];
    
}
- (void) animationEnded{
    self.userInteractionEnabled = YES;
    [oldTile removeFromSuperview];
    [oldTile release];
    oldTile = nil;
}

- (NSDate*) dateSelected{
    
    return [currentTile dateSelected];
}
- (NSDate*) monthDate{
    
    return [currentTile monthDate];
}
- (void) selectDate:(NSDate*)date{
    
    
    //TKDateInformation info = [date dateInformation];
    TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *month = [date firstOfMonth];
    
    if([month isEqualToDate:[currentTile monthDate]]){
        [currentTile selectDay:info.day];
        return;
    }else {
        
        if ([delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)] && ![delegate calendarMonthView:self monthShouldChange:month animated:YES] ) 
            return;
        
        if ([delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)] )
            [delegate calendarMonthView:self monthWillChange:month animated:YES];
        
        
        NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:month startOnSunday:sunday];
        NSArray *data = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
        TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithMonth:month 
                                                                              marks:data 
                                                                   startDayOnSunday:sunday];
        [newTile setTarget:self action:@selector(tile:)];
        [currentTile removeFromSuperview];
        [currentTile release];
        currentTile = newTile;
        [self.tileBox addSubview:currentTile];
        self.tileBox.frame = CGRectMake(0, 44, newTile.frame.size.width, newTile.frame.size.height);
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
        
        self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
        self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[date monthString],[date yearString]];
        [currentTile selectDay:info.day];
        
        if([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
            [self.delegate calendarMonthView:self monthDidChange:date animated:NO];
        
        
    }
}
- (void) reload{
    NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:[currentTile monthDate] startOnSunday:sunday];
    NSArray *ar = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
    
    TKCalendarMonthTiles *refresh = [[[TKCalendarMonthTiles alloc] initWithMonth:[currentTile monthDate] marks:ar startDayOnSunday:sunday] autorelease];
    [refresh setTarget:self action:@selector(tile:)];
    
    [self.tileBox addSubview:refresh];
    [currentTile removeFromSuperview];
    [currentTile release];
    currentTile = [refresh retain];
    
}

- (void) tile:(NSArray*)ar{
    
    if([ar count] < 2){
        
        if([delegate respondsToSelector:@selector(calendarMonthView:didSelectDate:)])
            [delegate calendarMonthView:self didSelectDate:[self dateSelected]];
        
    }else{
        
        int direction = [[ar lastObject] intValue];
        UIButton *b = direction > 1 ? self.rightArrow : self.leftArrow;
        
        NSDate* newMonth = [self dateForMonthChange:b];
        if ([delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)] && ![delegate calendarMonthView:self monthShouldChange:newMonth animated:YES])
            return;
        
        if ([delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)])					
            [delegate calendarMonthView:self monthWillChange:newMonth animated:YES];
        
        
        //clicking the grey button changes the month if the following code is not commented
        [self changeMonthAnimation:b];
        
        int day = [[ar objectAtIndex:0] intValue];
        
        
        // thanks rafael
        TKDateInformation info = [[currentTile monthDate] dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        info.day = day;
        
        NSDate *dateForMonth = [NSDate dateFromDateInformation:info  timeZone:[NSTimeZone timeZoneWithName:@"GMT"]]; 
        [currentTile selectDay:day];
        
        
        if([delegate respondsToSelector:@selector(calendarMonthView:didSelectDate:)])
            [delegate calendarMonthView:self didSelectDate:dateForMonth];
        
        if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
            [delegate calendarMonthView:self monthDidChange:dateForMonth animated:YES];
        
    }
    
}


- (UIImageView *) topBackground{
    if(topBackground==nil){
        //topBackground = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Grid Top Bar.png")]];
        topBackground = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"Month Grid Top Bar.png")]];
    }
    return topBackground;
}
- (UILabel *) monthYear{
    if(monthYear==nil){
        monthYear = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tileBox.frame.size.width, 38)];
        
        monthYear.textAlignment = UITextAlignmentCenter;
        monthYear.backgroundColor = [UIColor clearColor];
        monthYear.font = [UIFont boldSystemFontOfSize:22];
        monthYear.textColor = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
    }
    return monthYear;
}
- (UIButton *) leftArrow{
    
    if(leftArrow==nil){
        leftArrow = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        leftArrow.tag = 0;
        [leftArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        [leftArrow setImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Left Arrow"] forState:0];
        
        leftArrow.frame = CGRectMake(0, 0, 48, 38);
    }
    return leftArrow;
    
}
- (UIButton *) rightArrow{
    if(rightArrow==nil){
        rightArrow = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        rightArrow.tag = 1;
        [rightArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
        rightArrow.frame = CGRectMake(320-45, 0, 48, 38);
        
        
        
        [rightArrow setImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Right Arrow"] forState:0];
        
    }
    return rightArrow;
}
- (UIScrollView *) tileBox{
    if(tileBox==nil){
        tileBox = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, 320, currentTile.frame.size.height)];
    }
    return tileBox;
}
- (UIImageView *) shadow{
    if(shadow==nil){
        shadow = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Shadow.png")]];
    }
    return shadow;
}

@end
