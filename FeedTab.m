//
//  FeedTab.m
//  inTown
//
//  Created by ManGoes Mobile 2 on 6/9/11.
//  Copyright 2011 Mahbub Morshed. All rights reserved.
//

#import "FeedTab.h"
#import "Comment.h"
#import "FeedTabSub1.h"

@implementation FeedTab
@synthesize friendFeedTableView;
@synthesize myFeedTableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //[[self navigationItem] setTitle:@"Feed"];
        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Cooper Black" size:35];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
        label.text = NSLocalizedString(@"Feed", @"");
        [label sizeToFit];
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    // for aesthetic reasons (the background is black), make the nav bar black for this particular page
	self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    //self.navigationController.navigationBar = nil; 
	
	// match the status bar with the nav bar
	[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;
    
    //    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"  Post  " style: UIBarButtonItemStylePlain target:self action:@selector(postToServer) ]autorelease];
    
    [super viewDidLoad];
    
    
}

//-(void)postToServer{
//      // [[self navigationItem] setTitle:@"Comment"];
//           
//    UIViewController *targetViewController = [[[Comment alloc]init ] autorelease]; 
//    	[[self navigationController] pushViewController:targetViewController animated:YES];
//}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    //    self.navigationController.navigationBar. = ;
    //    [self.navigationController.navigationBar setHidesBackButton:NO animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int i=1;
    if(tableView== friendFeedTableView){
        
        i=1;
        // Return the number of sections.
        return i;
    }
    return i;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int i =1;
    if(tableView== friendFeedTableView){
        i=5;
        // Return the number of rows in the section.
        return i;
    }
    return i;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines = 0;
    }
    if(tableView== friendFeedTableView){
        cell.imageView.image= [UIImage imageNamed:@"profilepic.png"];
        
        cell.textLabel.text= @"Sankalpo Ghose just planned a trip to San Fransico from Jan 1 To Jan 3";
        cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:12];
        cell.textLabel.textColor= [UIColor purpleColor];
    }
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    UIViewController *targetViewController = [[[FeedTabSub1 alloc]init ] autorelease]; 
    //[self.tableView removeFromSuperview];
    [[self navigationController] pushViewController:targetViewController animated:YES];
}



@end
